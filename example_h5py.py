from phd import *
    

    
    # velocities histogram
    A,B=histogram(xv[:,3],density=True)
    for a in particleData.iterkeys():
        print a
    particleData.keys()[0]
    # time Step
    dset=f[f.keys()[2]]
    
    we=

def master():
    # data serial
    root='/storage/space2/phrlaz/HybridCode/hypsi/data/15AUG14/'
    name='data_serial_z0.hdf'
    f=openHDF(root+name)
    
    # data parallel
    root='/storage/space2/phrlaz/HybridCode/hypsi/data/'
    name='data_disp1d2x2_z0.hdf'
    f=openHDF(root+name)
    
    root='/storage/space2/phrlaz/'
    name='particle_alpha_z0.hdf'
    _f=openHDF(root+name)
        
    #check number of cell per axis
    nx, ny, nz = getCellNumber(f)    
    
    fieldAxis='Bz'
    nt=4002   
    dt=0.03
    # variation along x-axis
    #plt.figure()
    B_x=getField(f,fieldAxis,'x',1,1)       
    FFT(f,B_x,'x',1,1,nt,dt)    
    
    # variation along y-axis        
    #plt.figure()
    B_y=getField(f,fieldAxis,'y',75,1)   
    FFT(f,B_y,'y',75,1,nt,dt)

    # variation along x-axis
    #plt.figure()
    B_z=getField(f,fieldAxis,'z',75,1)   
    FFT(f, B_z,'z',75,1,nt,dt)   
    
    for i in range(5):
        plt.figure()
        Bz=getField(f,'Bz',10+i,10+i)   
        FFT(Bz,'Bz',10+i,10+i)  
          
    plt.figure()    
    plotParticles(f, 91)
    #plt.figure()
    #_B=getField(f,'Bz',120,60)   
    #FFT(_B,'Bz',115,115)

    getDomain([6.0,5.0,4.0])
    #_B=getField(f,'Bx',60,60)   
    #FFT(_B,'Bx',115,115)
#### byte
8*6*5280000/(1024*1024)