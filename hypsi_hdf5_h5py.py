#!/usr/bin/python2.7
# print the names of Groups and datatypes
import sys
import h5py
import numpy as np
import matplotlib.pyplot as plt
# HDF Hierarchy
def VisitAllObjects(Group,Path):
    FileInfo={}
    for i in Group.items():
        if isinstance(i[1],h5py.Group):
            VisitAllObjects(i[1],Path+'/'+i[0])
        else:
            DatasetName=Path+'/'+i[0]
            FileInfo[DatasetName]=(Group[DatasetName].shape,
                                   Group[DatasetName].dtype,
                                   Group[DatasetName].attrs.listitmes()
                                   )
# any HDF file: File, Group, and Dataset

#Files=sys.argv[1:] # get file name from screen
# select one file, create a map, datatypes using VisitAllObjects
Files='data_speed_z0.hdf'

FirstFid=h5py.File(Files,'r')

# start dictionary




VisitAllObjects(FirstFid,'')
FirstFid.close()

# print dataset paths and info to screen
for (k,v) in FileInfo.items():
    print k,v









