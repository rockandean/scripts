% Create figure
scrsz = get(0,'ScreenSize');
figure1 = figure('Position',[1 1  scrsz(3)*0.26 scrsz(4)*.638]);

% Create axes
axes1 = axes('Parent',figure1,'TickDir','in','PlotBoxAspectRatio',[1 1 1],...
    'LineWidth',2,...
    'FontWeight','bold',...    'xlim',[60.88 61.06],...[57.85 63.15],...    'ylim',[0 25],...     'YTick',0:10:20,...    'XTick',60.9:0.05:61.05,...    
    'FontSize',24);
% Uncomment the following line to preserve the X-limits of the axes
% xlim(axes1,[60.7 61]);
% Uncomment the following line to preserve the Y-limits of the axes
% ylim(axes1,[0 25]);
box(axes1,'on');
hold(axes1,'all');

% Where is data

c           = 2.9979e10  ;%              cm / sec
kB          = 1.3807e-16 ;%            erg /deg(K)
eV          = 1.6022e-12 ;%  erg
q           = 4.8032e-10 ;% 1.6022e-19   statcoulomb
B           = 5e-8*1e4   ;%              gauss
n           = 50      ;%              cm^-3
m_p         = 1.6726e-24; %              g
m_e         = 9.1094e-28; %              g
w_pe        = sqrt((4*pi*n* q*q)/m_e);  %   1/sec
w_pi        = sqrt((4*pi*n* q*q)/m_p);  %   1/sec
omega_e     = (q * B) / (m_e*c);        %   rad/sec
omega_i     = (q * B) / (m_p*c);        %   rad/sec
permeability= 1   ;                     % no units
mass_proton = 1.6726e-24 ;              %   g
V_a         = B/sqrt(permeability*n*mass_proton);
T_e         = 100 * eV ;          %(ratio  kB/ev) unit erg 
rho_e         = (sqrt(2*T_e/m_e)/omega_e)*1e-5; % km
rho_i         = (sqrt(2*T_e/m_p)/omega_i)*1e-5; % km

% Create plot
if true
    w=linspace(0,2*pi*50,1000);
    k=sqrt( (w/c).^2 - (w*w_pe^2)./(c^2*(w-omega_e)) - (w*w_pi^2)./(c^2*(w+omega_i)));
    w=w/omega_i;
    k=2*pi*real(k)*(V_a/omega_i);
    
    plot(k,w,'Parent',axes1,'MarkerSize',12,'Marker','.','LineStyle',':',...
    'Color',[0 0 1],...
    'DisplayName','raw data')
    % line of w_pe and w
    array_w_pe=w_pe*(1:100);
    %plot(array_w_pe)
    %plot([0,max(k)+max(k)/20],[ omega_e/omega_i omega_e/omega_i], 'k-')
    plot([0,max(k)+max(k)/20],[ 1 1], 'k-')   
    % plot V_a
    t = k;
    hline1 = plot(t,t,'k');
    % Create legend
    legend1 = legend(axes1,'show');
    set(legend1,...
    'Position',[0.604662077597002 0.817870632672333 0.0973091364205257 0.073300283286119]);
end