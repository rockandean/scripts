import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.fftpack import fft,ifft,fftshift
import h5py
from scipy import signal
# number of sample points
N = 512

# sample time
DT=1.0/4000.0
DX=1.0/3000.0
#
df=1/(DT*N)
fmax=1/(2.0*DT)
#
dk=1/(DX*N)
kmax=1/(2.0*DX)
#
t=np.r_[0.0: DT*N :DT]
x=np.r_[0.0:DX*N/2:DX]
#
freqw=np.r_[-N/2:N/2:df]*fmax
freqk=np.r_[-N/2:N/2:dk]*kmax
#
# create signal
rdn=np.random.random((t.shape[0],x.shape[0]))
B=np.empty((t.shape[0],x.shape[0]))
_A=np.empty(B.shape,dtype=complex)

for i in range(x.size):
    for j in range(t.size):
        #B[int(j),int(i)]=np.sin((2*np.pi)*50*x[i]+(2*np.pi)*150*t[j])+rdn[j,i]
        B[int(j),int(i)]=np.sin(300*x[i]-900*t[j])+rdn[j,i]

mpl.rcParams['mathtext.fontset']='stix'
# plot image
plt.figure()
plt.subplot(2,1,1)
im=plt.imshow(B,extent=[0.0, DX*N/2, 0.0, DT*N],aspect='auto',interpolation='none',origin='lower')
plt.colorbar(im,orientation='vertical')
plt.title('$\sin( 2\pi 50 + 2\pi 150) + \gamma$')
plt.xlabel('Position')
plt.ylabel('Time')

# Fourier Transform
_y=np.empty(B.shape,dtype=complex)
y=np.empty(B.shape,dtype=complex)

win=signal.hann(B.shape[0])
win2=signal.hann(B.shape[1])
for t in range(B.shape[0]):
    _y[t,:]=fftshift(fft(win2*B[t,:]))/N
for x in range(B.shape[1]):
    y[:,x] = fftshift(fft(win*_y[:,x]))/N
    
#y=fftshift(fft(fft(win*B,axis=0),axis=1))/N**2
#
plt.subplot(2,1,2)
Y=y*y.conjugate()
PSK=(Y.real)
im=plt.imshow(np.log(PSK),extent=[-kmax,kmax,-fmax,fmax],aspect='auto',interpolation='none')#,origin='lower')
plt.colorbar(im,orientation='vertical')
plt.axis([-kmax,kmax,-fmax,fmax])
plt.title('Dispersion relation plot')
plt.xlabel('k')
plt.ylabel('f [Hz]')
plt.ylim([0, fmax])
plt.xlim([0, kmax])
plt.show()
