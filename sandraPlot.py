#!/usr/bin/python2.7
from numpy import *
from matplotlib.pyplot import *
from scipy.fftpack import fft,fft2,fftshift,fftfreq,diff,convolve
import h5py
# read a box B_x(x, y=yo, z=zo, t)
# data_speed_output
# fft in space for x=4

########## README #############
# 1.- in main() funciton setup root string for the right folder first
###############################

def scratch():
    Bx=stitchFields('Bx',N_=4)
    Bxt=array([f['TimeStep/'+str(i)+'/ZoneFields/Bx'][j,0,0] for i in range(0,210,10) for j in range(10)])
    a=ones(27)
    a=a.reshape(3,3,3)
    a[0,:,0]=2
    a[0,:,1]=3
    a[1,:,1]=2
    a[1,1,:]=2
    a[0,1,:]=3
    a[2,:,:]=4
    a[2,1,:]=2
    b=a+3
    # a after b in x, axis=0, 
    concatenate((b,a),axis=0)
    # b after a in y, axis=1
    concatenate((a,b),axis=1)
    # b after a in z, axis=2
    concatenate((a,b),axis=2)
    #test for a concatenation, repeat twice in 2, +1,
    #copy once in y +2 and keep same z, then make a full copy on x +3 keep the rest the samecon 
    a[0]=[[1,2,3],[4,5,6],[7,8,9]]
    l1=concatenate((concatenate((a,a+1),axis=2),a+1),axis=2)
    l2=concatenate((concatenate((a+2,a+1),axis=2),a+1),axis=2)
    l3=concatenate((concatenate((a+3,a+1),axis=2),a+1),axis=2)
    # axis 1
    m1=concatenate((l1,l2),axis=1)
    m2=concatenate((l3,l2),axis=1)
    #axis 0
    M=concatenate((m1,m2),axis=0)
    # fft in time, needs to be per each points in x
    # keep it to one box right now
    f = openHDF()
    Bxt=array([f['TimeStep/'+str(i)+'/ZoneFields/Bx'][4,0,0] for i in range(0,210,10)])
    Bxt.resize(32)
    Y=fft(Bxt)
    w=2*pi*r_[0:16]/32
    plot(w,abs(Y[0:Y.size/2]))

def openHDF(_name):#,N=None):
    """
    Open the file _name so we can read from it.
    f=openHDF('31Jan2014/data_speed_output_z0.hdf')# n=0 default
    f=openDHF('31Jan2014/data_speed_output_z0.hdf',3) for the *z3.hdf files
    """
    #open output_hdf
    #if N is not None:
     #   N_ = N
    #else:
     #   N_= 0
    #name_ = '31Jan2014/data_speed_output_z0.hdf'
    #f_ = h5py.File(_name[:-5]+str(N_)+'.hdf','r')
    f_ = h5py.File(_name,'r')
    return f_

#hdf loop
def getMagneticField(fileName,foo,np):
    """
    Bx=getMagneticField('data_speed_z0.hdf','Bx')  #,Y0=0,Z0=0)
    """
    Bx={}
    Bx['x']=zeros(0)
    #includes read Y0,Z0 from function arguments
    Bx['y']=15
    Bx['z']=15
    #get nx from file
    nx=np[0]
    ny=np[1]
    nz=np[2]
    # can get z from y0,z0?
    z=1
    for j in range(z,nx*ny*nz,ny*nz):
        f=openHDF(fileName[:-5]+str(j)+'.hdf')
        print j
        y0=Bx['y']
        z0=Bx['z']
        Bx['x']=f['TimeStep/0/ZoneFields/'+foo][2:-2,y0,z0].reshape((f['TimeStep/0/ZoneFields/'+foo][2:-2,y0,z0].size,1))
        timeSteps=256
        timeStepsCadence=1
        for i in range(timeStepsCadence,timeSteps+timeStepsCadence,timeStepsCadence):
            Bx['x']=concatenate((Bx['x'],f['TimeStep/'+str(i)+'/ZoneFields/'+foo][2:-2,y0,z0].reshape((f['TimeStep/'+str(i)+'/ZoneFields/'+foo][2:-2,y0,z0].size,1))),1)
        if j==z: BxAux=Bx['x']
        if j>ny*nz: 
            Bx['x']=concatenate((BxAux,Bx['x']))
            BxAux=Bx['x']
    return Bx
def plot_component():
    # read a line of magneic field
    # plot it against time
    #   need time points, and for each the specific magnetic point
    return 0
    
def plot_dispersion(root):
    """"""
    Bx=getMagneticField(root+'input_file_54_z0.hdf','Bx',[6,3,3])
    F=fft2(Bx['x'])
    Y=F*F.conjugate()
    imshow(log10(Y.real))
    show()    
    
def main():
    root='/storage/space2/phrlaz/HybridCode/hypsi/data/22MAY14/'
    plot_dispersion(root)
