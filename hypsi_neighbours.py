#./python
"""
Created on Tue Oct 14 11:41:39 2014

@author: phrlaz

i=40
nxcn=6
nycn=3
nzcn=3
nyzcn=nycn*nzcn

ixcn=i/nyzcn
rem=i%nyzcn

iycn=rem/nzcn
izcn=rem%nzcn
#%%
for ix in [ixcn-1,ixcn, ixcn+1]:
    for iy in [iycn-1,iycn, iycn+1]:
        for iz in [izcn-1,izcn, izcn+1]:
            print (ix+nxcn)%nxcn*nyzcn +(iy+nycn)%nycn*nzcn + (iz+nzcn)%nzcn
            
#%% Inverse transform method
"""
import matplotlib.pyplot as plt
import numpy as np

rng=100

x = np.log (rng)

#a,b=plt.hist(rng)
plt.hist(rng)
#c,d=plt.hist(x)


plt.hist(x)
#plt.plot(x)

#%% Acceptance-Rejection Method

N=9000
#vmag=np.zeros(N)
vx=np.zeros(N)
vy=np.zeros(N)

vther=1.5
for i in range(N):
    
    #print 1.0-0.99999999999*a,'', 0.99999999999*a, '',a

    #print np.sqrt(-np.log(1.0-0.99999999999*a))
    vmag=vther*np.sqrt(-2*np.log(1.0-0.99999999999*np.random.random()))
    a=np.random.random()
    vx[i]=4+vmag*np.sin(2*np.pi*a)
    vy[i]=5+vmag*np.cos(2*np.pi*a)
    
#plt.subplot()  
#plt.plot(vy,vx+4000,'.')
#plt.hist(vmag,100)
#plt.figure()
plt.subplot()
plt.hist(vx,100)
#plt.figure()
plt.subplot()
plt.hist(vy,100)
plt.figure()
plt.hist(np.random.randn(N),10)

#%% test for zone init

N=200000

#vmag=np.zeros(N)

Lx=8
Ly=10
x0=4
y0=5
vther=1
j=0
k=0
Ninitx=10000
Ninity=10000
ktest=False
jtest=False
#
vx1=np.zeros(Ninitx)
vy1=np.zeros(Ninitx)
vx2=np.zeros(Ninity)
vy2=np.zeros(Ninity)
#for i in range(N):    # create a particle position
while not ktest or not jtest:
    vmag1=vther*np.sqrt(-2*np.log(1.0-0.99999999999*np.random.random()))
    a=np.random.random()
    vxx=x0+vmag1*np.sin(2*np.pi*a)
    vyy=y0+vmag1*np.cos(2*np.pi*a)
#
    print j,k, jtest,ktest
    if not jtest and vxx>=x0 and vyy<=y0:                   # Upper half  , left side
        vx1[j]=vxx
        vy1[j]=vyy
        j+=1
#
        if j==Ninitx:
            jtest=True
#        elif  jtest and ktest:         break
    elif not ktest and vxx>=x0 and vyy>y0:
        vx2[k]=vxx
        vy2[k]=vyy
        k+=1
#        
        if k==Ninity:
            ktest=True 
#        elif  jtest and ktest:         break            
# 
plt.subplot(3,2,1)
plt.plot(vy1,vx1,'.')   
plt.subplot(3,2,3)
plt.hist(vx1,100)
plt.subplot(3,2,5)
plt.hist(vy1,100)      
#
plt.subplot(3,2,2)
plt.plot(vy2,vx2,'.')   
plt.subplot(3,2,4)
plt.hist(vx2,100)
plt.subplot(3,2,6)
plt.hist(vy2,100)   
plt.show()
#%%
"""
    else if:
        vmag2=vther*np.sqrt(-2*np.log(1.0-0.99999999999*np.random.random()))
        a=np.random.random()
        vx[i]=x0+vmag2*np.sin(2*np.pi*a)
        vy[i]=y0+vmag2*np.cos(2*np.pi*a)   
    
plt.subplot()  
"""
#%%
nodes = 36 # 36, 256
numberOfParticles = int(100*20) # 100*6, 100*0.85
cellsize=0.04444
N = nodes * numberOfParticles  # 256 * 40
 #vmag=np.zeros(N)
vx=np.zeros(N)
vy=np.zeros(N)

vther=[17.776, 17.776, cellsize*22] #0.4444 :0.78559  #  dx*25:1.111, make 10 larmor radius 
print 500*np.sqrt(2*np.pi)*vther[2]
L_unit = 0.07200
print "FWHM",2*np.sqrt(2*np.log(2))*vther[2]/0.1111, "Diameter: Larmor radius units"
#21.24 for exact diameter 20
# 1 larmor adius = 0.1111 delta _i
"""
find sigma , 20.0/2*np.sqrt(2*np.log(2)) for a 20\rho_i
1.774100225154747/0.1111
ans/
"""

print vther[2]/0.1111, "radius Based on sigma: "
for i in range(N):
   vmag=vther[2]*np.sqrt(-2*np.log(1.0-0.99999999999*np.random.random()));
   a=np.random.random();
   vx[i]=vther[0]+vmag*np.sin(2*np.pi*a);
   vy[i]=vther[1]+vmag*np.cos(2*np.pi*a);


 #plt.subplot()  
#plt.plot(vy,vx+4000,'.')
 #plt.hist(vmag,100)
 #plt.figure()
#plt.subplot()
#plt.hist(vx,100)
 #plt.figure()
#plt.subplot()
data=vx;
binWidth= cellsize;
bins = np.arange(vther[1]-3*vther[2], vther[1]+3*vther[2] + binWidth, binWidth);
plt.hist(vx,bins);

#%% create a random 2d hist
# Need nodes, cellsize, ratio larmor/skin depth, blob_radius
nodes = 16 #  256:4x4 36:3x3 Remember this is for the plane, times number of cells
nparticles=100
numberOfParticles =  int(nparticles*60)# 100*6, 100*0.85, 2130 one core
cellsize=0.1016
grid=100
ratioskindepth=1.01615275867
blob_radius   =   7.78
"""0.04657
36,int(100*60) cellsize=0.1111
36,int(100*400) cellsize = 0.04444
256,int(100*55)
256,
"""
N = nodes * numberOfParticles  # 256 * 40

vther=[cellsize*grid/2, cellsize*grid/2, cellsize*blob_radius] #0.4444 :0.78559  #  dx*25:1.111, make 10 larmor radius 
#print 500*2*np.pi*vther[2]*vther[2]
#   this is blob_radius_a vther[2]
print "box size:  ", cellsize*grid, cellsize*grid
print "FWHM",2*np.sqrt(2*np.log(2))*vther[2]/ratioskindepth, "Diameter: Larmor radius units"
print "FWHM 2D",2*np.sqrt(np.log(2))*vther[2]/ratioskindepth, "Diameter: Larmor radius units"
#21.24 for exact diameter 20
# 1 larmor adius = 0.1111 delta _i
"""
find sigma , 20.0*0.1111/(2*np.sqrt(2*np.log(2))) for a 20\rho_i
find factor cellsize::  20.0*0.1111/(2*np.sqrt(2*np.log(2))*cellsize)
ans/
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

x = np.random.normal(vther[0], vther[2], N)
y = np.random.normal(vther[0], vther[2], N)
binWidth= cellsize;
bins = np.arange(vther[1]-3*vther[2], vther[1]+3*vther[2] + binWidth, binWidth);
H, xedges, yedges = np.histogram2d(y, x, bins=(bins, bins))

fig = plt.figure(figsize=(7, 3))
ax = fig.add_subplot(131)
ax.set_title('imshow: equidistant')
im = plt.imshow(H, interpolation='nearest', origin='low',
                extent=[xedges[0], xedges[-1], yedges[0], yedges[-1]])

ax = fig.add_subplot(132)
ax.set_title('pcolormesh: exact bin edges')
X, Y = np.meshgrid(xedges, yedges)
ax.pcolormesh(X, Y, H)
ax.set_aspect('equal')             

ax = fig.add_subplot(133)
ax.set_title('NonUniformImage: interpolated')
im = mpl.image.NonUniformImage(ax, interpolation='bilinear')
xcenters = xedges[:-1] + 0.5 * (xedges[1:] - xedges[:-1])
ycenters = yedges[:-1] + 0.5 * (yedges[1:] - yedges[:-1])
im.set_data(xcenters, ycenters, H)
ax.images.append(im)
ax.set_xlim(xedges[0], xedges[-1])
ax.set_ylim(yedges[0], yedges[-1])
ax.set_aspect('equal')
plt.show()
plt.figure()
plt.hist(x,bins);
print "n_peak::",np.max(H)
