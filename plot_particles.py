
#from phd import *
#%%
#from mayavi.mlab import *
import h5py
import yt
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft,fftshift
#%
class simParams:
    """
    sm=simParams(f)
    it include all simulation values
    """
    dt=0
    dxdydzcell=0
    electron_gamma=0
    hostzone=0
    initial_Te=0
    npsets=0
    substeps=0
    nxnynzdomain=0
    nzones=0
    prng_seed=0
    pscharge=0
    psname=0
    uniform_resis=0
    dxdydz=0
    nxnynz=0
    region=0
    xyz_domain=0
    zone=0
    #
    geometry=0
    nt=0
    fieldCadence=0
    def __init__(self,_f):
        simParams.dt             = _f['/SimulationParameters/dt'][0] # float
        simParams.nt             = _f['/SimulationParameters/nt'][0] # int
        simParams.fieldCadence   = _f['/SimulationParameters/fieldCadence'][0] # int 
        simParams.dxdydzcell     = _f['/SimulationParameters/dxdydzcell'] # array-like [:]
        simParams.electron_gamma = _f['/SimulationParameters/electron_gamma'][0] # float
        simParams.hostzone       = _f['/SimulationParameters/hostzone'][0] # float 
        simParams.initial_Te     = _f['/SimulationParameters/initial_Te'][0] # float 
        simParams.npsets         = _f['/SimulationParameters/npsets'][0] # float 
        simParams.nsubsteps      = _f['/SimulationParameters/nsubsteps'][0] # float 
        simParams.nxnynzdomain   = _f['/SimulationParameters/nxnynzdomain'] # array-like [:]
        simParams.nzones         = _f['/SimulationParameters/nzones'][0] # float 
        simParams.prng_seed      = _f['/SimulationParameters/prng_seed'][0] # float 
        simParams.pscharge       = _f['/SimulationParameters/pscharge'][0] # float 
        try:        
            simParams.psname         = _f['/SimulationParameters/psname'][0]
            simParams.npdata        = _f['ParticleData/timestep_0/sample_0/pset_0/npdata'][0]
        except:
            pass
        simParams.uniform_resis  = _f['/SimulationParameters/uniform_resis'][0] # float 
        simParams.dxdydz     = _f['/SimulationParameters/zinfo/dxdydz'] # array-like [:] 3
        simParams.nxnynz     = _f['/SimulationParameters/zinfo/nxnynz'] # array-like [:] 3
        simParams.region     = _f['/SimulationParameters/zinfo/region'] # array-like [:] 6
        simParams.xyz_domain = _f['/SimulationParameters/zinfo/xyz_domain'] # array-like [:] 6
        simParams.zone       = _f['/SimulationParameters/zinfo/zone'][0] # int this procese 
        simParams.geometry     = [int(self.xyz_domain[1]/self.region[1]),
                                int(self.xyz_domain[3]/self.region[3]),
                                int(self.xyz_domain[5]/self.region[5])]
    def getCellNumber(self):
        """
        nxnynzdomain = simParams.getCellNumber(f)
        """
        print ' x cell size domain:', self.nxnynzdomain[0]-1
        print ' y cell size domain:', self.nxnynzdomain[1]-1
        print ' z cell size domain:', self.nxnynzdomain[2]-1
        
    def readExtras(self,_nt,_fieldCadence):
        """
        simParams.readExtras(3600,3)
        """        
        simParams.nt            = _nt # int
        simParams.fieldCadence  = _fieldCadence # int
        
    def getField(self,_f,_fieldName,_axisName,axisLineA,axisLineB):
        """
        getField read a 2d array that contains a plane over time, for a given position [Xo,Yo,Zo].
        If uses axisName=X then Yo,Zo must be given or any other combination.
        Options are {Bx, By, Bz}, {Ex, Ey, Ez}. Use getCellNumber(f) to see the limits of
        each direction. Example:
            
        Bx=simParams.getField(f,'Bx','x',15, 15)    #where f is an HDF object
        """
        rootName=str(_f.filename)
        debbug = 0
        #
        if debbug:          

            _root = '/storage/space2/phrlaz/HybridCode/hypsi/data/17SEPT14/'
            _name = 'data_serial_z0.hdf'
            _f   = h5py.File(_root+_name,"r")
            rootName=str(_f.filename)        
            #_B=zeros((geometry[0]*nxnynz[0],_time.size))
            #_A=empty(_B.shape, dtype=complex)
            _fieldName = 'Bx'
            _axisName = 'x'
            axisLineA = 1
            axisLineB = 1


        ## time
        timeStep = _f['TimeStep']
        list     = [int(a) for a in timeStep.iterkeys()]
        list.sort()
        ## simulation parameters
        half=1
       # newCadence = self.fieldCadence
        newCadence = 3
        _time        = np.array(list[1:])[:int(np.size(list[1:])/half)]
        _time        = _time[::newCadence]
        cadence      = _time[1]-_time[0]
        # test if simParams match timeStep
        if _time.size!=self.nt:
            print 'nt mismatch, must run readExtras'
            simParams.nt = int(list[-1])        
        if cadence != self.fieldCadence:
            print 'field cadence mismatch, run readExtras'
            simParams.fieldCadence = cadence
            

        if np.array(self.geometry).prod()!=self.nzones:
            print('Error: Nzones and geometry do not match!')

        ## Pick axis: offset and axisLineA,axisLineB to avoid readings for too many hdf files
        if  _axisName == 'z':
            offset=1 # read up, no offset        
            _B=np.zeros((_time.size,self.geometry[2]*self.nxnynz[2])) #e.g.zeros(120,512)
            ax=np.array(range(self.nxnynzdomain[0])).reshape((self.geometry[0],self.nxnynz[0]))
            by=np.array(range(self.nxnynzdomain[1])).reshape((self.geometry[1],self.nxnynz[1]))
            #
            for i in range(self.geometry[0]):
                for j in range(self.geometry[1]):
                    # print 'ERROR find first hdf file'
                    if (ax[i,:] == axisLineA).any() & (by[j,:] == axisLineB).any():
                        # any test for any True
                        cont = i*self.geometry[1]*self.geometry[2]+j*self.geometry[2]
                        print 'Position:', j ,i,' Starting node: ', cont 
                        axisLineA_=axisLineA-i*self.nxnynz[0]
                        axisLineB_=axisLineB-j*self.nxnynz[1]
                        
            print 'Starting node: ', cont             
            for k in range(self.geometry[2]):
                print 'Loop: ', k+1, '/', self.geometry[2]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time:
                    _B[int((t-1)/cadence),k*self.nxnynz[2]:self.nxnynz[2]*(k+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][axisLineA_,axisLineB_,1:-1]
                cont+=offset
            #cont=cont2   # if ever change time loop again out  
            
        elif _axisName == 'y':
            offset=self.geometry[2] # only z is under y
            _B=np.zeros((_time.size,self.geometry[1]*self.nxnynz[1])) #e.g.zeros(120,512)
            ax=np.array(range(self.nxnynzdomain[0])).reshape((self.geometry[0],self.nxnynz[0]))
            bz=np.array(range(self.nxnynzdomain[2])).reshape((self.geometry[2],self.nxnynz[2]))
            #
            for i in range(self.geometry[0]):
                for k in range(self.geometry[2]):
                    #if cont > offset:
                    # print 'ERROR find first hdf file'
                    if (ax[i,:] == axisLineA).any() &  (bz[k,:] == axisLineB).any():
                        cont=k+i*self.geometry[1]*self.geometry[2]#contAux
                        print 'Position:', k ,i
                        axisLineA_=axisLineA-i*self.nxnynz[0]
                        axisLineB_=axisLineB-k*self.nxnynz[2]
    
            print 'Starting node: ', cont 
            for j in range(self.geometry[1]):
                print j
                print 'Loop: ', j+1, '/', self.geometry[1]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time:
                    _B[int((t-1)/cadence),j*self.nxnynz[1]:self.nxnynz[1]*(j+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][axisLineA_,1:-1,axisLineB_]
                cont+=offset
            #cont=cont2   # if ever change time loop again out 
                
        elif _axisName == 'x':
            offset=self.geometry[1]*self.geometry[2]
            _B=np.zeros((_time.size,self.geometry[0]*self.nxnynz[0])) #e.g.zeros(120,512)
            ay=np.array(range(self.nxnynzdomain[1])).reshape((self.geometry[1],self.nxnynz[1]))
            bz=np.array(range(self.nxnynzdomain[2])).reshape((self.geometry[2],self.nxnynz[2]))
            #
            for j in range(self.geometry[1]):
                for k in range(self.geometry[2]):
                    #if cont > offset:
                    # print 'ERROR find first hdf file'
                    if (ay[j,:] == axisLineA).any() &  (bz[k,:] == axisLineB).any():
                        cont=j*self.geometry[2]+k
                        print 'Position:', k ,j
                        axisLineA_=axisLineA-j*self.nxnynz[1]
                        axisLineB_=axisLineB-k*self.nxnynz[2]
    
            print 'Starting node: ', cont
            for i in range(self.geometry[0]):
                print 'Loop: ', i+1, '/', self.geometry[0]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time: 
                        _B[int((t-1)/cadence),i*self.nxnynz[0]:self.nxnynz[0]*(i+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][1:-1,axisLineA_,axisLineB_]
                cont+=offset
            #cont=cont2   # if ever change time loop again out
        else:
            print 'Error to read field direction'
        #    
        return _B
        
    def plotUniformSpace(self,_f,_t):
        """
        plotUniformSpace(f,0)
        """
        #
        from mpl_toolkits.mplot3d import Axes3D
        debbug = 0
        if debbug:          
            _root = '/storage/space2/phrlaz/HybridCode/hypsi/data/22SEPT14/f/'
            _name = 'data_serial_z0.hdf'
            _f   = h5py.File(_root+_name,"r")
            rootName=str(_f.filename)
            _t=0
        npdata = _f['ParticleData/timestep_0/sample_0/pset_0/npdata'][0]      
        _XX=np.zeros((npdata,6))
        _XX =_f['ParticleData/timestep_'+str(_t)+'/sample_0/pset_0/pdata']    
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        xs=_XX[:,0]
        ys=_XX[:,1]
        zs=_XX[:,2]
        ax.scatter(xs, ys, zs, c='r', marker='o')
        
        ax.set_xlabel('$X/(v_A/\Omega_i)$ ')
        ax.set_ylabel('$Y/(v_A/\Omega_i)$ ')
        ax.set_zlabel('$Z/(v_A/\Omega_i)$ ')
        plt.title(' made with plotUniformSpace in plot_particles.py')
        plt.show()
        #plot(_XX[:,0],_XX[:,1],'.') 
        #plot(_XX[:,0],_XX[:,2],'.') 
        #plot(_XX[:,1],_XX[:,2],'.')
        #plt.axis([0,_dx*4,0,_dx*4])      
        #plt.xlabel('$v_A/\Omega_i$')
        #plt.ylabel('$v_A/\Omega_i$')
    def plotvhist(self,_f):
        #
        debbug = 0
        if debbug:
            _root='/storage/space2/phrlaz/gitRepos/benhypsi/hypsi/'
            _name='data_testUniform_z0.hdf'
            _f   = h5py.File(_root+_name,"r")
            rootName=str(_f.filename)
            self=simParams(_f)
            
            
        #for t in range(0,self.nt,200):
        _v= _f['ParticleData/timestep_0/sample_0/pset_0/pdata']
        mys=['x position', 
            'y position',
            'z position',
            'vx',
            'vy',
            'vz']
        plt.figure()
        #print np.mean(_v[:,5])
        for i in np.arange(3,6):
            plt.subplot(1,3,i-2)
            plt.hist(_v[:,i],100)#,density=True)
            plt.xlabel(mys[i])  
              
#def get_demo_image():
#    import numpy as np
#    from matplotlib.cbook import get_sample_data
#    f = get_sample_data("axes_grid/bivariate_normal.npy", asfileobj=False)
#    z = np.load(f)
#    # z is a numpy array of 15x15
#    return z, (-3,4,-4,3)
#    
#def demo_simple_grid(fig):
#    """
#    A grid of 2x2 images with 0.05 inch pad between images and only
#    the lower-left axes is labeled.
#    fig=plt.figure()
#    demo_simple_grid(fig)
#    """
#    from mpl_toolkits.axes_grid1 import AxesGrid
#    grid = AxesGrid(fig, 131, # similar to subplot(141)
#                    nrows_ncols = (3, 1),
#                    axes_pad = 0.05,
#                    label_mode = "1",
#                    )
#
#    Z, extent = get_demo_image()
#    for i in range(4):
#        im = grid[i].imshow(Z, extent=extent, interpolation="nearest")
#
#    # This only affects axes in first column and second row as share_all = False.
#    grid.axes_llc.set_xticks([-2, 0, 2])
#    grid.axes_llc.set_yticks([-2, 0, 2])
#%%        
def plotParticles(_f,tend,thisParticle):
    """
    This function Assumes Bfield is in x-direction.
    for t in range(0,5):
        plotParticles(f,180,37000+1000*t)
    """
    #
    debbug = 0
    if debbug:    
        _root='/storage/space2/phrlaz/gitRepos/benhypsi/hypsi/'
        _name='data_testUniform_z0.hdf'
        _f   = h5py.File(_root+_name,"r")
        rootName=str(_f.filename)
        thisParticle=35005
        tend =200
             
    # time 
    _particleData=_f['ParticleData']
    list = [ int(a[9:]) for a in _particleData.iterkeys()]
    list.sort()
    
    ## simulation parameters
    half=1
    _time = np.array(list[:tend])[:int(np.size(list[:tend])/half)]    
    cadence  = _time[1]-_time[0]     
    #print _time    
    #position
    npdata = _f['ParticleData/timestep_0/sample_0/pset_0/npdata'][0]
    xyz_domain = _f['/SimulationParameters/zinfo/xyz_domain'] # array-like [:] 6
    dxdydz = _f['/SimulationParameters/zinfo/dxdydz'] # array-like [:] 6
    #pdata  = [ _f['ParticleData/timestep_'+str(a)+'/sample_0/pset_0/pdata'][thisParticle] for a in _time ]
    _xv=np.zeros((_time.size,6))
    for t in _time:
    #    print t
        _xv[int((t)/cadence),:]= _f['ParticleData/timestep_'+str(t)+'/sample_0/pset_0/pdata'][thisParticle]
#
    x = _xv[:,0]
    y = _xv[:,1]
    z = _xv[:,2]
    #plt.plot(_time,z,'o-g')
    #plt.plot(x,z,'.')
    #tend=20

    #plt.plot(y[:tend]-y[:tend].mean(),z[:tend]-z[:tend].mean(),'.-k')
    plt.plot(y[:tend],z[:tend],'.-')
    plt.axis((xyz_domain[2],xyz_domain[3],xyz_domain[4],xyz_domain[5]))
    plt.grid(True)
    plt.xticks([i for i in np.arange(0,xyz_domain[3],dxdydz[1])])
    plt.yticks([i for i in np.arange(0,xyz_domain[5],dxdydz[2])])
    #plt.axis([0,20,0,20])
    #plt.gca().set_xticks(np.arange(0,20,0.5))
    #plt.gca().set_yticks(np.arange(0,20,0.5))    
    #plt.gca().grid(True)
    plt.xlabel('$r_gi/\Omega_i$')
    plt.ylabel('$r_gi/\Omega_i$')
    plt.title('B=1 ; dx=0.5rho_i T=975eV')

    #plt.plot(x,y,'.g')
    #plot3d(x, y, z, tube_radius=0.025,  colormap='Spectral')#
    #t=64
    #plt.figure()
#%%
def plot3dParticles(self,_f,tend,thisParticle):
    """
    plot3dParticles(f,12,4)
    """
    debbug = 0
    if debbug:          
        _root = '/storage/space2/phrlaz/HybridCode/hypsi/data/25SEPT14/a/'
        _name = 'data_serial_z0.hdf'
        _f   = h5py.File(_root+_name,"r")
        rootName = str(_f.filename)
        thisParticle = 4
        tend = 24
        self = simParams(f)
    import matplotlib as mpl
    from mpl_toolkits.mplot3d import Axes3D      
    mpl.rcParams['legend.fontsize'] = 10
    
    # time 
    _particleData=_f['ParticleData']
    list = [ int(a[9:]) for a in _particleData.iterkeys()]
    list.sort()
    
    ## simulation parameters
    half=1
    _time = np.array(list[:tend])[:int(np.size(list[:tend])/half)]    
    cadence  = _time[1]-_time[0]     
    #print _time    
    #position
    npdata = _f['ParticleData/timestep_0/sample_0/pset_0/npdata'][0]
    #pdata  = [ _f['ParticleData/timestep_'+str(a)+'/sample_0/pset_0/pdata'][thisParticle] for a in _time ]
    _xv=np.zeros((_time.size,6))
    for t in _time:
    #    print t
        _xv[int((t)/cadence),:]= _f['ParticleData/timestep_'+str(t)+'/sample_0/pset_0/pdata'][thisParticle]
#
    x = _xv[:,0]
    y = _xv[:,1]
    z = _xv[:,2]
    
    fig = plt.gcf()
    ax = fig.gca(projection = '3d')

    ax.plot(x, y, z, )#label='parametric curve'
    plt.hold(True)
    ax.legend()
    ax.set_xlim3d(0.2, self.xyz_domain[1]/3)
    ax.set_ylim3d(0, self.xyz_domain[3])
    ax.set_zlim3d(0, self.xyz_domain[5])
    ax.set_xlabel('x ($v_A/\Omega_i$)')
    ax.set_ylabel('y ($v_A/\Omega_i$)')
    ax.set_zlabel('z ($v_A/\Omega_i$)')
    plt.show()
###########################################################        
    # data parallel
    #root='/storage/space2/phrlaz/HybridCode/hypsi/data/'
    #name='data_disp1d2x2_z0.hdf'
#%%
#root='/storage/space2/phrlaz/gitRepos/benhypsi/hypsi/'
#name='data_test_T03_z0.hdf'

root='/storage/space2/phrlaz/HybridCode/hypsi/data/d2015/AUG/03/'
name='data_Blobs3D_z0.hdf'
f=h5py.File(root+name,'r')



#%% check number of cell per axis
sm = simParams(f)

#%%read missing nt and field cadenceslc.show()
#sm.readExtras(360,10)

# print cell number
sm.getCellNumber()

#
sm.plotvhist(f)

# Used to plot uniform space or initial space configuration at t
#sm.plotUniformSpace(f,0)

#%% this can be used to plot many particles in the same plot

for i in np.arange(1,10):
    plotParticles(f,5000,80080+800*i)
    
#%% plot one particle in 3D
#plt.figure() 
for i in np.arange(1,2):
    plot3dParticles(sm,f,10000,15400+i*8000+i*4000)
    

#%%
plt.close('all')
########################## SCRAP ###################### 

#%% read density? using yt


# load into yt

bbox = np.array([[-1.5, 1.5], [-1.5, 1.5], [-1.5, 1.5]])
ds = yt.load_uniform_grid(data, dn.shape, length_unit="Mpc", bbox=bbox, nprocs=64)



slc = yt.SlicePlot(ds, "z", ["density"])
slc.show()

#%% Slice plot ala matlab
%matplotlib qt
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


dn=f['TimeStep/0/Pset/0/ZoneMoments/dn'][:,:,:]
data = dict (density = (dn, "dimensionless"))
fig = plt.figure()
ax = fig.gca(projection='3d')
x = np.linspace(0,136,136)
X, Y = np.meshgrid(x,x)
levels = np.linspace(0, 1, 10)
#ax.contourf(X,Y,dn[1:-1,1:-1,0],zdir='z')
#ax.contourf(X,dn[1:-1,1:-1,0],Y,zdir='y',offset=4)
ax.contourf(X,Y,0.1*dn[1:-1,1:-1,0],zdir='z',offset=4)
ax.contourf(X,dn[1:-1,1:-1,0],Y,zdir='y',offset=4)
#surf = ax.plot_surface(X,Y,dn[1:-1,1:-1,0],rstride=1, cstride=1, cmap=cm.coolwarm,
      #  linewidth=0, antialiased=False)
#ax.set_zlim(-1.01, 1.01)
#surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
        #linewidth=0, antialiased=False)
#ax.set_zlim(-1.01, 1.01)
ax.set_xlim3d(0, self.xyz_domain[1])
ax.set_ylim3d(0, self.xyz_domain[3])
ax.set_zlim3d(0, self.xyz_domain[5])
#fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()