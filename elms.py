#!/bin/env/ python
""" ELMs analysis routines and more """
#hola
#import numpy as np
from numpy import *
import matplotlib as mp
from matplotlib.pyplot import *
from matplotlib.mlab import * 
#import matplotlib.pyplot as plt
import sys
from plots import *
# solution for naming in mat files; check saveData in dataJet.git
def fixName(nameXXX):
    #key='data_80800_DD/V5-VLD3<ADC'
    key=nameXXX
    char='_'
    charSet=['/','-','<']
    for n,k in enumerate(charSet):
        key=key.replace(k,char)
    return key

def getBite(dat_,timeList_):
    """ usage:
    ndat=getBite(dat,[60.5, 61.0])
    """
    a=dat_[:,0]>timeList_[0]
    b=dat_[:,0]<timeList_[1]
    ndat_=dat_[a & b]
    ti_=find(dat_[:,0]>timeList_[0])
    tf_=find(dat_[:,0]<timeList_[1])
    return ndat_,ti_[0],tf_[-1]
    #start reading time and get slice for each key name save the slice in a
    #new variable xnew=x[timeSlice].copy() for each key , return
def catchELM(dat,ndat_,margin=0.5e-3):
    """ it catches ELMs in the region ndat_ by using margin as the max
    ELM time width
    catchELM(ndat,)
    """
    margin=0.5e-3
    debbug=True
    #ndat_=ndat
    mu=np.mean(ndat_[:,1])
    sigma=np.std(ndat_[:,1])

    # shift is too match the index with the original data 
    shift=find(dat[:,0]==ndat_[0,0])
    # loop till ELMs are smaller than < u+2*s
    r=max(ndat_[:,1])
#    while (r>mu+3.5*sigma):
    for k in range(2):
        #ndat_=ndat
        aux=find(ndat_[:,1]==r)
        index=aux[0]+shift[0]
        a=ndat_[aux[0],0]-margin<ndat_[:,0]
        b=ndat_[:,0]<ndat_[aux[0],0]+2*margin
        zone=(a & b)
        if debbug:
            copydat=ndat_
            copydat[zone,1]=mu+3*sigma
            plot(dat[:,0],dat[:,1],'b')
            plot(copydat[zone,0],copydat[zone,1],'k',dat[index,0],dat[index,1],'or')
        ndat_[zone,1]=mu+sigma
        plot(ndat_)
        r=max(ndat_[:,1])

    
def getElms(mat,name,X):
    """ function saves all ELMs
    mat,keys=loadMat('/home/andino/Documents/ELMs/57872.mat')
    getElms(mat, 'data_57872_AD36',[60.0, 61.0])
    """
    # it catchs all ELMs in the chunks passed in X
    debbug=True
    #name='data_57872_AD36'
    #X=[60.0, 61.0]
    dat=mat[name]
    # Loop for X
    for i in np.arange(len(X)-1):
        ndat,ti,tf=getBite(dat,[X[i],X[i+1]])
        catchELM(dat,ndat,margin=0.5e-3)

def scratch2():
    """
    fft of VLD2 and VLD3, for the zone were the disruption ocurr 
    """
    from scipy.fftpack import fft,fft2,fftshift,fftfreq,diff,convolve
    mat,key=loadMat('/home/andino/Documents/ELMs/57872.mat')
    #matPlot(mat,key,64.4,64.8)
    t_start=64.4 
    t_end=64.8
    dat3=mat[key[6][0]]
    VLD3=getBite(dat3,[t_start, t_end])
    dat2=mat[key[5][0]]
    VLD2=getBite(dat2,[t_start, t_end])
    VLD2=VLD2[1:,:]
    # frequency sampled
    fs=1/mean(VLD2[1:,0]-VLD2[:-1,0])
    # time of signal
    T=VLD2[-1,0]-VLD2[0,0]
    n_points=int(round(fs*T))+1
    # plot signal
    subplot(2,1,1)
    t=linspace(VLD2[0,0],VLD2[-1,0],n_points)
    plot(t,VLD2[:,1])
    N=2**11
    delta=N-VLD3.shape[-2]
    # there is an easy way to pad using fft
    #A_=zeros((N,2))
    #A_=pad(VLD2,((0,delta),(0,0)),'constant',constant_values=(0,0))
    # this window will smooth the edges out
    window=hamming(n_points)
    # fft with N will pad zeros till N
    signal=VLD2[:,1]-mean(VLD2[:,1])
    Y=8/3*fftshift(fft(signal*window))
    Y2=Y*Y.conjugate()
    mag=((Y2.real))
    freq=linspace(-0.5,0.5,n_points)
    # add fft to plot
    subplot(2,1,2)
    plot(freq,mag)
    ylabel('Magnitude')
    xlabel('frequency')
    #max(Y2.real[N/2:])
    #from numpy.fft import fft, fftshift
    #window = np.hamming(51)
    #plt.plot(window)
    #plt.title("Hamming window")
    #plt.ylabel("Amplitude")
    #plt.xlabel("Sample")
    #plt.show()
   # 
    #plt.figure()
    #A = fft(window, 2048) / 25.5
    #mag = np.abs(fftshift(A))
    #freq = np.linspace(-0.5, 0.5, len(A))
    #response = 20 * np.log10(mag)
    #response = np.clip(response, -100, 100)
    #plt.plot(freq, response)
    #plt.title("Frequency response of Hamming window")
    #plt.ylabel("Magnitude [dB]")
    #plt.xlabel("Normalized frequency [cycles per sample]")
    #plt.axis('tight')
    #plt.show()    
    
def scratch():
    """
    get ELMs from reading a signal and start having chunks
    then search for each elm in a smaller region with max(),
    then move to next window, using the range [a,b] and the 
    vector of coordinates as middle points to create smaller ranges
    """
    mat,keys=loadMat('/home/andino/Documents/ELMs/57872.mat')
    matPlot(mat,keys)
    matPlot(mat,keys,56.8,64)
    name=keys[4][0]

    dat=mat[name]

    plot(dat[:,0],dat[:,1])

    a=dat[:,0]>60.5

    b=dat[:,0]<61.0

    ndat=dat[a & b]

    plot(ndat[:,0],ndat[:,1],'r')

    r=max(ndat[:,1])

    rr=dat[:,1]==r

    plot(dat[rr],'or')

    plot(dat[rr][0][0],dat[rr][0][1],'or')

    margin=1e-3

    a=dat[:,0]>dat[rr][0][0]-margin

    b=dat[:,0]<dat[rr][0][0]+margin

    nndat=dat[a & b]

    plot(nndat[:,0],nndat[:,1],'.k')

    nndat[:,1]=mean(ndat[:,1])

    plot(nndat[:,0],nndat[:,1],'.k')

    nndat[:,1]+=std(ndat[:,1])

    plot(nndat[:,0],nndat[:,1],'.k')
