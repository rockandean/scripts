%% HDF Reader
% Modified Pete's script.

%%% Data file locations
%dest = '/storage/space2/phrlaz/HybridCode/hypsi/data/d2015/MAR/27MAR15/'

%FEB/20FEB15/'
%MAR/27MAR15/'
clear all
dest = '/storage/space2/phrlaz/HybridCode/hypsi/data/d2015/AUG/04/c/';
%dest = '/storage/space2/phrlaz/gitRepos/benhypsi/hypsi/run/'; % Data file path
fileprefix = 'data_Blobs3D'; % Data file root name
%fileprefix = 'data_blob2d_241';
%What do you want me to read?
readB = true; % B field
readE = true; % E field
readM = false; % Particle moments n, v

% Field dump cadence (timesteps between data dumps)
% Note: currently assumes the same cadence for all dumped fields
%fcadence = 1000;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Read file zero for domain parameters
filename = strcat(fileprefix,'_z0.hdf');
fullpath = strcat(dest,filename);
ns = h5read(fullpath,'/SimulationParameters/npsets');
psname= h5read(fullpath,'/SimulationParameters/psname');
%ns = 1 %// trick to read matrix in    
nz = h5read(fullpath,'/SimulationParameters/nzones');    
dx = h5read(fullpath,'/SimulationParameters/zinfo/dxdydz');
nd = double(h5read(fullpath,'/SimulationParameters/nxnynzdomain'));
ts_info = h5info(fullpath,'/TimeStep');
nt = max(str2num(strrep(strcat(ts_info.Groups.Name),'/TimeStep/',' ')));
%nt = cast(h5read(fullpath,'/SimulationParameters/nt'),'double');
dt = h5read(fullpath,'/SimulationParameters/dt');
fcadence =cast( h5read(fullpath,'/SimulationParameters/fieldCadence'),'double');



region = zeros(nz,6);
region(1,:) = h5read(fullpath,'/SimulationParameters/zinfo/region');
%
% Read all other zone files for region information
for p=1:nz-1;
    filename = strcat(fileprefix,'_z',num2str(p),'.hdf');
    fullpath = strcat(dest,filename);
    region(p+1,:) = h5read(fullpath,'/SimulationParameters/zinfo/region');   
end

% Convert region data to processor cell limits
rcell = int32([region(:,1)/dx(1)+1 ...
         region(:,2)/dx(1)   ...
         region(:,3)/dx(2)+1 ...
         region(:,4)/dx(2)   ...
         region(:,5)/dx(3)+1 ...
         region(:,6)/dx(3)]);

rcell=double(rcell);
clear region
     
% Initialise field variables for full simulation domain

if readB==true, 
    blank = zeros(nd(1),nd(2),nd(3),ceil(nt/fcadence(1)));
    B = struct('x',blank,'y',blank,'z',blank);
end;
if readE==true, 
    blank = zeros(nd(1),nd(2),nd(3),ceil(nt/fcadence(2)));
    E = struct('x',blank,'y',blank,'z',blank);
end;

% Initialise moment variables for full simulation domain
if readM==true,

        % Find the name of each species for use in the particle moment
        % struct. For example: M.(pname).vx
        pname=strsplit(psname,',');

end
%
if readM==true,
    blank = zeros(nd(1),nd(2),nd(3),ceil(nt/fcadence(3)));
    for p=0:ns-1
        %p=0
        %pname ={'proton'};
        M.(pname{p+1}) =  struct('vx',blank,'vy',blank,'vz',blank,'n',blank,'nB',blank);
    end
end
clear blank

dims = 'xyz';
momu = {'Vx' 'Vy' 'Vz' 'dn' 'dnB'};
moml = {'vx' 'vy' 'vz' 'n' 'nB'};
%%  read only n not vx, vy,vz, for k=1:4#
% i= is also to much, so only readiing some i's
%for j=1:length(fcadence)
    
BCadence=fcadence(1);
ECadence=fcadence(2);
nCadence=fcadence(3);

%nt=36000
%newCadence=4000
%laststr = 0;
bar=waitbar(0,'loading ...');
%msg_old = 'Reading_000%\n';
for p=1:nz
    waitbar(double(p)/double(nz))
    %for i=1:newCadence:(nt+1)
        % Display how far through the reading process we are
        %msg = strcat('Reading_',num2str(100*(i+(p-1)*nt)/(nz*nt),'%3.3i'),'%%\n');
        % This next bit deletes the old message to save space in the command window
        %if (~strcmp(msg,msg_old))
        %fprintf(repmat('\b',1,laststr));
        %fprintf(msg);
        %laststr = numel(msg)-2;
        %end
        %msg = msg_old;
        %waitbar(double(i)/double(nt+1))
        filename = strcat(fileprefix,'_z',num2str(p-1),'.hdf');
        fullpath = strcat(dest,filename);
        
        % Read zone field into temporary variable, then copy into the
        % larger field, missing out ghost cells
        for i=1:BCadence:(nt+1);
        for k = 1:3;
            if readB==true,
                temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/ZoneFields/B',dims(k)));
                B.(dims(k))(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/BCadence)) ...
                    = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
            end
        end
        end
        for i=1:ECadence:(nt+1);
        for k =1:3;
            if readE==true,
                temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/ZoneFields/E',dims(k)));
                E.(dims(k))(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/ECadence)) ...
                    = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
            end
        end % field loop
        end
        if readM==true;
            for i=1:nCadence:(nt+1);
            % Do the same for the moments, looping over each particle species
            for s=0:ns-1
                for k=1:5
                    temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/Pset/',num2str(s),'/ZoneMoments/',momu{k}));
                    M.(pname{s+1}).(moml{k})(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/nCadence)) ...
                        = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
                end % moment loop
            end % species loop
            end
        end
        
end % zone loop
%end
close(bar)
clear temp
%% Constants gaussian units
% Blob

c           = 2.9979e10  ;%              cm / sec
kB          = 1.3807e-16 ;%            erg /deg(K)
eV          = 1.6022e-12 ;%  erg
q           = 4.8032e-10 ;% 1.6022e-19   statcoulomb
Bval        = 0.25*1e4   ;%              gauss        %%%%%%%%% change here
n           = 2e19*1e-6      ;%              cm^-3           %%%%%%%%% change here
m_p         = 1.6726e-24; %              g
m_e         = 9.1094e-28; %              g
w_pe        = sqrt((4*pi*n* q*q)/m_e);  %   rad/sec
w_pi        = sqrt((4*pi*n* q*q)/m_p);  %   rad/sec
omega_e     = (q * Bval) / (m_e*c);        %   rad/sec
omega_i     = (q * Bval) / (m_p*c);        %   rad/sec
permeability= 1   ;                     % no units
             
V_a         = Bval/sqrt(4*pi*n*m_p);
T_e         = 80*1.6022e-19/1.3807e-23 * kB ;     %(ratio  kB/ev) unit erg %%%%%%%%% change here
rho_e       = (sqrt(T_e/m_e)/omega_e); % km
rho_i       = (sqrt(T_e/m_p)/omega_i); % km
%
%    Bblob     =   0.4*1e4  ;         % gauss
%    n_blob    =   1e19/1e6 ;          % cm^-3
%    Tblob     =   4e6      ;       % Kelvin
%ionGyro = 1.02e2*sqrt(T_e)/(Bval)*1e-2;  % m
%% smooth plots
% - recharge differents perp and par files using reading section.
% - make subplots
% _ add waves according to 
dt = h5read(fullpath,'/SimulationParameters/dt');
dx = h5read(fullpath,'/SimulationParameters/zinfo/dxdydz');
parallel=0;
perpendicular=0;
% Create figure
scrsz = get(0,'ScreenSize');
%figure1 = figure('Position',[1 1  scrsz(3)*0.26 scrsz(4)*.638]);

% Create axes
%axes1 = axes('Parent',figure1,'TickDir','in','PlotBoxAspectRatio',[1 1 1],...
%    'LineWidth',2,...
%    'FontWeight','bold',...    'xlim',[60.88 61.06],...[57.85 63.15],...    'ylim',[0 25],...     'YTick',0:10:20,...    'XTick',60.9:0.05:61.05,...    
%    'FontSize',24);
% Uncomment the following line to preserve the X-limits of the axes
% xlim(axes1,[60.7 61]);
% Uncomment the following line to preserve the Y-limits of the axes
% ylim(axes1,[0 25]);
%box(axes1,'on');
%hold(axes1,'all');
tCadence=1
xCadence=1
NN=50
%nt=1000
nt = max(str2num(strrep(strcat(ts_info.Groups.Name),'/TimeStep/',' ')));
% create FFT
fieldSelection = squeeze(E.y(:,1,20,1:NN));  % B is (x,t) and E is (x,t) as opposite to kspace

Ntime=ceil(nt/BCadence);  %% plus t=0
Nspace =  ceil(nd(1)/xCadence); % nd(2) for perp
cellSize = 0.102;
time=zeros(1,Ntime);
%H = sigwin.hann(Ntime);
%win=generate(H);
win=hann(Ntime,'periodic'); % ensure DFT/FFT use and return an N-object

% k space k(t,x)  --> (N, domain(i))
kspace=zeros(Ntime, Nspace);

for tt=2:Ntime
    
    kspace(tt-1,:) = fftshift( fft( fieldSelection(:,tt) ) );
end

PSK= kspace.*conj(kspace);

% k-w space
% FS fourier space (w,k)
FS=zeros(Ntime,Nspace);

for kk=1:Nspace 
    FS(:,kk) = fftshift( fft (win.*kspace(:,kk),Ntime) );
end
dx=cellSize*xCadence
PS = FS.*conj(FS);
% fourier space coordinates
Dk = 2*pi/(Nspace*dx);
kmax = pi/(dx);
kAxis = (0:Dk:kmax-Dk); % Normalization 2pi required by Hypsi code

dt=dt*tCadence;
%Dw = 1/(dt*Ntime);
Dw = 2*pi/(dt*Ntime);
wmax = pi/(dt);
wAxis = (0:Dw:wmax-Dw); % no required Normalization, already in Omega_i units
% limits
Ta=floor(Ntime/2)+1;
Tb=Ntime;

Ka=floor(Nspace/2)+2;
Kb=Nspace;

Alog =  log10(PS);
imagesc(kAxis,wAxis,Alog(Ta:Tb,Ka:Kb))
axis xy
colorbar
xlabel('k (V_a/\Omega_i)')
ylabel('\omega/\Omega_i ')
title('Parallel to Bx,variation of By')

% Create plot functions
if parallel
    % whistlers
    w=linspace(0.1,10*pi,1000);
    k=sqrt( 1 - w_pe^2./(w.*(w-omega_e))  - w_pi^2./(w.*(w+omega_i))).*w/c;
    k=real(k)*V_a;
    hold on
    plot(k,w,'MarkerSize',12,'LineStyle','-',...  
        'Color',[0 0 1],...  
        'DisplayName','raw data')


    %
    w=w(1:end);
    k=sqrt( 1 - w_pe^2./(w.*(w+omega_e))  - w_pi^2./(w.*(w-omega_i))).*w/c;
    w=w;
    k=real(k)*V_a;
    hold on    
    plot(k,w,'MarkerSize',12,'LineStyle','-',...  
        'Color',[0 0 1],...  
        'DisplayName','raw data')
    % line of w_pe and w
    array_w_pe=w_pe*(1:100);
    %plot(array_w_pe)
    %plot([0,max(k)+max(k)/20],[ omega_e/omega_i omega_e/omega_i], 'k-')
    plot([0,max(k)+max(k)/10],[ 1 1], 'k-')   

    % plot V_a
    t = k;
    hline1 = plot(t,t,'k');
    % Create legend
    %legend1 = legend(axes1,'show');
    %set(legend1,...
    %'Position',[0.604662077597002 0.817870632672333 0.0973091364205257 0.073300283286119]);
end
if perpendicular
    % magnetoacustic or fast quasi -tranverse ordinary mode
    w=linspace(0.1,2*pi*5,10);
    k=sqrt( 1 + w_pi^2/(omega_i)^2 ).*w/c;
    w=w/omega_i;
    k=real(k)*(V_a/omega_i);
    hold on
    %plot(k,w,'MarkerSize',12,'LineStyle','-',...  
    %    'Color',[0 0 1],...       
    %    'DisplayName','raw data')

    plot([0,10],[ 1 1], 'k-')  
    lh=sqrt(omega_e*omega_i*(omega_i^2+w_pi^2)/(w_pi^2+omega_i*omega_e));
    plot([0,10],[ lh/omega_i lh/omega_i], 'k-') 
    % plot V_a
    %t = w;
    %hline1 = plot(t,t,'k');
    % Create legend
    %legend1 = legend(axes1,'show');
    %set(legend1,...
    %'Position',[0.604662077597002 0.817870632672333 0.0973091364205257 0.073300283286119]);    
%saveas(gca,'myplot.png');
end
%% 2D plot
t=0;
cellSize=0.04444;

figure
density =  squeeze(M.protons.nB(:,:,:,t)+M.test1.nB(:,:,:,t));
%Bfield = squeeze(B.z(:,:,:,:));
%clims = [ 0.8 max(M.protons.nB(:)+M.test1.nB(:)) ]
%xx=(1:810)*cellSize;
%yy=(1:810)*cellSize;
xx=(1:nd(2));
yy=(1:nd(3));
tt=(1:ceil(nt/fcadence(3)))*dt;
imagesc(xx,yy,density')
axis xy
ylabel('y /\rho_i')
xlabel('x /\rho_i')
title('n')