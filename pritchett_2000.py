# Prichett 2000 section II
# it follows a useful calculation in cgs (gaussian units) and MKS or SI 

from scipy import *

from numpy import sqrt,pi
def Va_CGS(_Bfield, _n):
    """Alfven Speed CGS"""
    permeability    =   1    # 
    mass_proton     =   1.6726e-24      # g
    return _Bfield/sqrt(4*pi*permeability*_n*mass_proton)
def Va_MKS(_Bfield, _n):
    """Alfven Speed MKS"""
    permeability    =   4*pi*1e-7    # H m^-1
    mass_proton     =   1.6726e-27      # kg
    return _Bfield/sqrt(permeability*_n*mass_proton)
#%% Courant----------------------- incresing velocity
def f(v,dt,dx,n):
    print v*dt,'<',dx/sqrt(n)
v = 0.02
dt = 0.01
dx = 0.04444
n = 3
for i in range(1,11):
    f(v+i/10.0,dt,dx,n)
    
#%% Courant whistlers dimensionless
def f(dt,dx,n):
    print dt,'<',dx**2/(sqrt(3)*pi)
dt = 0.002
dx = 0.04444
n = 3  # number of dimension -3D
for i in range(0,11): # for increasing dx
    #print dx+i/10.0
    f(dt,dx+i/10.0,n)
for i in range(0,11): # for increasing dt
    #print dx
    f(dt+i/100000.0,dx,n)    
#%% ---------------------------- 
# leapfrog 
w = 10000
dt = 0.001
print w*dt, '<', 2
### 
# Hybrid CFL condition ---------
dt = 0.001
dx = 0.2
n = 3
dt, '<', (dx)**2/(sqrt(n)*pi)
### ----------------------------
#%% This test comes from Winske, pritchett

Bfield  = 0.25*1e4 # Tesla*1e4--> gauss
density = 2e19*1e-6    # m^-3*1e-6 --> cm^-3
Temperature = 80 # eV

# CGS
c = 2.9979e10 #              cm / sec

k = 1.3807e-16 #            erg /deg(K)

n = density #  0.5 #                   cm^-3

q = 4.8032e-10 #         statcoulomb

B =  Bfield #2e-9*1e4 #                     gauss

m_p = 1.6726e-24  #         g

m_e =  9.1094e-28 #         g

eVtoErg =  1.6022e-12

T_e =  (Temperature*eVtoErg)     #       deg(K)

W_pi =  sqrt( ( 4*pi*n* q**2 )/m_p ) # rad/sec

W_pe =  sqrt( ( 4*pi*n* q**2 )/m_e ) # rad/sec

Omega_i = (q * B) / (m_p*c) # rad/sec

# i) c/W_pi in cm
print  'Ion skin depth:  ',"{:.4e}".format(c/W_pi/100), 'm'  # will see in km

# ii) rho = sqrt(T_e/m_p)/Omega_i in cm
print 'Ion gyroradius:  ',"{:.4e}".format((sqrt(T_e/m_p)/Omega_i)/100), 'm'  # will see in km

# iii) ion gyro-period in sec
print 'Ion gyro-period: ',"{:.4e}".format(1/(Omega_i/(2*pi))), 'sec' #

# iv) c/W_pe in cm

print 'Elec skin depth: ',"{:.4e}".format(c/W_pe/100), 'm' 

# iv) Alfven speed
print 'Alfven Speed:    ',"{:.4e}".format( Va_CGS(B,n)/100), 'm/sec' 



#%% MKS from Wesson

Bfield  = 0.25    # Tesla
density = 2e19 #0.5*1e6  # m^-3
Temperature = 80 # eV

# MKS
c = 2.9979e8 #              m / sec

e_o = 8.8542e-12 #          F / m

k = 1.3807e-23 #            J / K

n = density #0.5 * 1e6 #             m^-3

q = 1.6022e-19 #            Coulomb

B = Bfield #2e-9 #                  Tesla

m_p =  1.6726e-27 #         kg

m_e =  9.1094e-31 #         kg

eVtoJ = 1.6022e-19 #

T_e =  Temperature*eVtoJ #  K

W_pi =  sqrt( ( n* q**2 )/(m_p*e_o) ) # 1/sec

W_pe =  sqrt( ( n* q**2 )/(m_e*e_o) ) # 1/sec

Omega_i = (q * B) / (m_p) # 1/sec

# i) c/W_pi in m
print 'Ion skin depth:  ',"{:.4e}".format(c/W_pi), 'm'  # will see in km

# ii) rho = sqrt(T_e/m_p)/Omega_i in m
print 'Ion gyroradius:  ',"{:.4e}".format((sqrt(T_e/m_p)/Omega_i)), 'm' # will see in km

# iii) ion GYro-period in sec 
print 'Ion gyro-period: ',"{:.4e}".format(1/(Omega_i/(2*pi))), 'sec' #

# iv) c/W_pe in m
print 'Elec skin depth: ',"{:.4e}".format(c/W_pe),'m'

# v) Alfven speed
print 'Alfven Speed:    ', "{:.4e}".format(Va_MKS(B,n)), 'm/sec' 
#%%%
# Compare to fusion plasma

Bfield  = 0.4    # Tesla
density = 1e19  # m^-3
Temperature = 4e6* 1.3807e-23/1.6022e-19 # Kelvin *k_B/e --> eV
# MKS
c = 2.9979e8 #              m / sec

e_o = 8.8542e-12 #          F / m

k = 1.3807e-23 #            J / K

n = density #0.5 * 1e6 #             m^-3

q = 1.6022e-19 #            Coulomb

B = Bfield #2e-9 #                  Tesla

m_p =  1.6726e-27 #         kg

m_e =  9.1094e-31 #         kg

eVtoJ = 1.6022e-19 #

T_e =  Temperature*eVtoJ #  K

W_pi =  sqrt( ( n* q**2 )/(m_p*e_o) ) # 1/sec

W_pe =  sqrt( ( n* q**2 )/(m_e*e_o) ) # 1/sec

Omega_i = (q * B) / (m_p) # 1/sec


# i) c/W_pi in m
print 'Ion skin depth:  ',"{:.4e}".format(c/W_pi), 'm'  # will see in km

# ii) rho = sqrt(T_e/m_p)/Omega_i in m
print 'Ion gyroradius:  ',"{:.4e}".format((sqrt(2*T_e/m_p)/Omega_i)), 'm' # will see in km

# iii) ion GYro-period in sec 
print 'Ion gyro-period: ',"{:.4e}".format(1/(Omega_i/(2*pi))), 'sec' #

# iv) c/W_pe in m
print 'Elec skin depth: ',"{:.4e}".format(c/W_pe),'m'

# v) Alfven speed
print 'Alfven Speed:    ', "{:.4e}".format(Va_MKS(B,n)), 'm/sec' 
