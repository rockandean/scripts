%% Get RAM according to Minerva per Node
% https://wiki.csc.warwick.ac.uk/twiki/bin/view/Main/MinervaUserGuide#Resource_limits_and_queues
% Maximum memory per node: 24,150 MB (ppn=12,pvmem=2012mb) 
ram = 24144/12;
% Box dimension +2 ghost cells

NP              =    100;           % Number particles
NPblob          =    150000;        % get this from input_file
geometry        =   [16, 4, 4];
sim_domain      =   [400, 100, 100];

N = sim_domain(1)/geometry(1);
M = sim_domain(2)/geometry(2);
P = sim_domain(3)/geometry(3);

% Any grid field

grid = (N+2)*(M+2)*(P+2)*8;  % 8 Bytes

% Any grided value
B = grid*3*2;            % Bx, By, Bz;  B, Bhalf, two copies
E = grid*3;              % Ex, Ey, Ez
V = grid*4*2;              % Vx, Vy, Vz, n, two copies
% Particles: plasma, Blob

numParticles =  NP*6*8*N*M*P;     % N*M*P cells, 6 dimen, 8 Bytes
numBlob      =  NPblob*6*8;       % 6 dimen, 8 Bytes
numPar       =  numParticles+numBlob;
%% total

total= B + E + V + numPar;
totalMB=(total/1024)/1024;  % in MB

% me da 136MB de 2012mb no tiene sentido! se me cae el codigo por
% bad_alloc()
%% total no particles
field_nt= 1000 + 1;
n_nt=50 + 1;

totalfields= B + E ;
totalDensity=V ;% two copies ; two species 

totalMB=(((totalfields*field_nt+totalDensity*n_nt)/1024)/1024)/1024*256; % in GB
display(totalMB);