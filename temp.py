#file to be used with ipython
from numpy import *
from matplotlib.pyplot import *
from scipy.fftpack import fft,fft2,fftshift,fftfreq,diff,convolve
from mpl_toolkits.mplot3d import Axes3D
from mayavi import mlab
import h5py

def test():
    N = 64
    n = r_[0:N]
    w = 2*pi/N

    x = sin(n*w)+sin(n*3*w)+rand(N)
    stem(x)
    XX = fft(x)

    m = abs(XX)
    p = angle(XX)
    iz = find(abs(m<1))
    #p[iz] = zeros(iz.size)
    subplot(2,1,1); stem(m)
    subplot(2,1,2); stem(p)

# HDF Hierarchy
def visitAllObjects(Group,Path):
    FileInfo={}
    for i in Group.items():
        if isinstance(i[1],h5py.Group):
            visitAllObjects(i[1],Path+'/'+i[0])
        else:
            DatasetName=Path+'/'+i[0]
            FileInfo[DatasetName]=(Group[DatasetName].shape,
                                   Group[DatasetName].dtype)
    return FileInfo
def openHDF(name_,N=None):
    """
    f=openHDF('31Jan2014/data_speed_output_z0.hdf') n=0 default
    f=openDHF('31Jan2014/data_speed_output_z0.hdf',3) for the *z3.hdf files
    """
    #open output_hdf
    if N is not None: 
        N_ = N
    else:
        N_= 0
    #name_ = '31Jan2014/data_speed_output_z0.hdf'
    for i in arange(N_+1):
        f_ = h5py.File(name_[:-5]+str(i)+'.hdf','r')
    return f_
def readHDF(f_,str_):
    dat_=f_[str_]
    return dat_
    #read data. B
    #in x
    #in t
    #get a line to plot k/t
    #plot vy/vx  
def magnetic():
    f = openHDF()
    Bxt=[f['TimeStep/'+str(i)+'/ZoneFields/Bx'][4,0,0] for i in range(0,210,10)]
    Y=fft(Bxt)
    plot(abs(Y))
def electric():
    f = openHDF()
    Ext=[f['TimeStep/'+str(i)+'/ZoneFields/Ex'][4,0,0] for i in range(0,210,10)]
    Y=fft(Ext)
    plot(abs(Y))
def stitchFields(foo,N_):
    """  
    Bx=stitchFields('Bx',N_=4)
    Only for Zone fields this far
    """
    A_={}
    #N_=4
    for i in range(N_):
        f = openHDF(i)
        A_[i]=f['TimeStep/10/ZoneFields/'+foo]
    a_=A_[0][...]
    b_=A_[1][...]
    B_=concatenate((b_,a_),axis=2)
#   Bx0 in z=1 see HDFview
    A_.keys()
    A_[0][:,:,1]
    return B_

#    print f
#    name='TimeStep/'+int(i)+'/ZoneFields/Bx'
#    [Bx['TimeStep/'+int(i)+'/ZoneFields/Bx'] for i in range(0,200,10)]
#    Bxt=[f['TimeStep/'+str(i)+'/ZoneFields/Bx'][4,0,0] for i in range(0,210,10)]

#    Bx=f[name]
    #Bx[4,0,0]
#    Nx=Bx[:,0,0].size
#    Y=fft(Bxt)
#    plot(Y)
#    show()
#Bx=stitchFields('Bx',N_=4)
#    pdata=f['ParticleData/timestep_0/sample_0/pset_0/pdata']
#    xx=pdata[::400,0]
#    yy=pdata[::400,1]
#    zz=pdata[::400,2]
#   vx=pdata[:,3]
#    vy=pdata[:,4]
#    vz=pdata[:,5]
#    mlab.points3d(xx,yy,zz)
#plot
def scratch():    
    fig = plt.figure()
    ax = Axes3D(fig)
    a=array([2,2,2,2,1,1,1,1])
    a=a.reshape(2,2,2)
    x,y,z=a.nonzero()
    ax.scatter(x,y,z,c='red',marker='o', s=20)
    x=x+0.1
    ax.scatter(x,y,z,c='green',marker='o', s=20)
    #
def go():
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #fig = plt.figure()
    #ax = Axes3D(fig)
    ax.set_xlabel('X');ax.set_ylabel('Y');ax.set_zlabel('Z')
    n=3
    a0=ones(n**n)
    a0=a0.reshape(n,n,n)
    x0,y0,z0=a0.nonzero()
    ax.scatter(x0,y0,z0,c='green',marker='o', s=20)
    z1=z0+n
    ax.scatter(x0,y0,z1,c='red',marker='o', s=20)
    y1=y0+n
    ax.scatter(x0,y1,z0,c='blue',marker='o', s=20)
    ax.scatter(x0,y1,z1,c='yellow',marker='o', s=20)

def test_field():
    from mayavi.mlab import *
    import numpy as np
    Bx=stitchFields('Bx',N_=4)
    x,y,z=np.mgrid[:10,:10,:20]
    u=Bx.take(x)
    v=Bx.take(y)
    w=Bx.take(z)
    obj = quiver3d(x, y, z, u, v, z, line_width=1, scale_factor=.1)
    return obj
def test_quiver3d():
    from mayavi.mlab import *
    x, y, z = numpy.mgrid[-2:3, -2:3, -2:3]
    r = numpy.sqrt(x ** 2 + y ** 2 + z ** 4)
    u = y * numpy.sin(r) / (r + 0.001)
    v = -x * numpy.sin(r) / (r + 0.001)
    w = numpy.zeros_like(z)
    obj = quiver3d(x, y, z, u, v, w, line_width=3, scale_factor=1)
    return obj
