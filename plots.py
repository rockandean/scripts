#!/bin/env/ python
# utf-8 encoding

""" Plotting routines and functions """

""" loadMat('file.mat')
        Open a mat-file and removes non-data keys, passing data into a dictionary and also the keys.
    createFig()
        create a new figure of size [10, 8]inch, with random number ID between (1,999999).
    matPlot(mat,keys)
        read a dictionary-style, and the keys for data and names
    getBite(timeSlice,matVar)
        read a mat-file from loadMat() and return the time slice for all the signals
    plotMaster()
        test for module
"""
import numpy as np
import matplotlib as mp
import matplotlib.pyplot as plt
import sys
import scipy.io as sio

def loadMat(fileName_):
    """ usage:
    mat,key=loadMat('file.mat')
    """
    mat_=sio.loadmat(fileName_)
    #print ' _Header_, _version_, and _globals_ are not passed'
    """ remove non data keys """
    del mat_['__version__']
    del mat_['__header__']
    del mat_['__globals__']
    return mat_,sio.whosmat(fileName_)

def createFig():
    # might update to read size
    """ usage:
    fig=createFig()
    """
    mp.rcParams['figure.figsize'] = [10, 8]# set the default figure size
    figID_=int(abs(np.random.randn(1))[0]*100000)
    return plt.figure(figID_)

def matPlot(matVar_,matKey_,t1_=None,t2_=None):
    """ usage:
    matPlot(mat,key)
    matPlot(mat,key,55.5,65.2)
    """
    #nTotal=key[l][1][0]# total number of points in each signal
    #read how many signals into nRows
    number_of_plots=len(matVar_)
    # might need to plot indicated signal later
    #cases 1 nothing just plot
    #cases 2 plot one vertical one horizontal
    #cases 3 plot vertical
    #cases 4 plot vertical and 2x2
    #cases 5 plot vertical
    #cases 6 3x2 2x3 vertical
    #cases 7 8 9 10 11 12 
    #
    # check for nearest square
    #10 sqrt(nRows)
    nRows=number_of_plots
    #nRows=
    nCols=1 # might depend on nRows/k
    number_of_cols=1
    number_of_rows=nRows
    
    fig=createFig()
    plt.subplots_adjust(hspace=0.8)# make a little extra space between the subplots
    
    #    fig.add_subplot(nRows,nCols,1)
    if number_of_cols*number_of_rows== number_of_plots:
        for j in range(number_of_cols):
            for i in range(number_of_rows):
                plt.subplot(nRows,nCols,(i+1)+j)
                time_=matVar_[matKey_[i][0]][:,0]
                signal_=matVar_[matKey_[i][0]][:,1]
                plt.title(matKey_[i][0]).set_fontsize(10)
                #plt.xlabel('time')
                if t1_ is not None: plt.xlim((t1_,t2_))
                plt.plot(time_,signal_)

    else:
        print 'matPlot error, fail to match plot-array size'

def getBite(dat_,timeList_):
    """ usage:
    ndat=getBite(dat,[60.5, 61.0])
    """
    a=dat_[:,0]>timeList_[0]
    b=dat_[:,0]<timeList_[1]
    ndat_=dat_[a & b]
    return ndat_
    #start reading time and get slice for each key name save the slice in a
    #new variable xnew=x[timeSlice].copy() for each key , return

def plotMaster():
    """ test for module """
    mat,key=loadMat('/home/andino/Documents/ELMs/57872.mat')
    #mat,key=loadMat('57873.mat')
    matPlot(mat,key,57,64)