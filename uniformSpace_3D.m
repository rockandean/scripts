%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% http://www.mathworks.co.uk/help/matlab/ref/slice.html
% http://www.mathworks.co.uk/help/matlab/visualize/exploring-volumes-with-slice-planes.html
%
% HYPSI field file stitcher
%
% Script combines field dumps for several processors (e.g.
% data_bench_z0-9.hdf) into field variables such as B.x, E.x, M.protons.vx
% etc.
%
% Written by P W Gingell
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
% Data file locations
clear all
dest = '/storage/space2/phrlaz/HybridCode/hypsi/data/d2015/AUG/06/';
%dest = '/storage/space2/phrlaz/gitRepos/benhypsi/hypsi/run/'; % Data file path
fileprefix = 'data_Blobs3D'; % Data file root name


%What do you want me to read?
readB = true; % B field
readE = true; % E field
readM = false; % Particle moments n, v

% Field dump cadence (timesteps between data dumps)
% Note: currently assumes the same cadence for all dumped fields
%fcadence = 1000;
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Read file zero for domain parameters
filename = strcat(fileprefix,'_z0.hdf');
fullpath = strcat(dest,filename);
ns = h5read(fullpath,'/SimulationParameters/npsets');
psname= h5read(fullpath,'/SimulationParameters/psname');
%ns = 1 %// trick to read matrix in    
nz = h5read(fullpath,'/SimulationParameters/nzones');    
dx = h5read(fullpath,'/SimulationParameters/zinfo/dxdydz');
nd = double(h5read(fullpath,'/SimulationParameters/nxnynzdomain'));
ts_info = h5info(fullpath,'/TimeStep');
nt = max(str2num(strrep(strcat(ts_info.Groups.Name),'/TimeStep/',' ')));
%nt = cast(h5read(fullpath,'/SimulationParameters/nt'),'double');
dt = h5read(fullpath,'/SimulationParameters/dt');
fcadence =cast( h5read(fullpath,'/SimulationParameters/fieldCadence'),'double');




region = zeros(nz,6);
region(1,:) = h5read(fullpath,'/SimulationParameters/zinfo/region');
%
% Read all other zone files for region information
for p=1:nz-1;
    filename = strcat(fileprefix,'_z',num2str(p),'.hdf');
    fullpath = strcat(dest,filename);
    region(p+1,:) = h5read(fullpath,'/SimulationParameters/zinfo/region');   
end

% Convert region data to processor cell limits
rcell = int32([region(:,1)/dx(1)+1 ...
         region(:,2)/dx(1)   ...
         region(:,3)/dx(2)+1 ...
         region(:,4)/dx(2)   ...
         region(:,5)/dx(3)+1 ...
         region(:,6)/dx(3)]);

rcell=double(rcell);
clear region
     
% Initialise field variables for full simulation domain

if readB==true, 
    blank = zeros(nd(1),nd(2),nd(3),ceil(nt/fcadence(1)));
    B = struct('x',blank,'y',blank,'z',blank);
end;
if readE==true, 
    blank = zeros(nd(1),nd(2),nd(3),ceil(nt/fcadence(2)));
    E = struct('x',blank,'y',blank,'z',blank);
end;

% Initialise moment variables for full simulation domain
if readM==true,

        % Find the name of each species for use in the particle moment
        % struct. For example: M.(pname).vx
        pname=strsplit(psname,',');

end
%
if readM==true,
    blank = zeros(nd(1),nd(2),nd(3),ceil(nt/fcadence(3)));
    for p=0:ns-1
        %p=0
        %pname ={'proton'};
        M.(pname{p+1}) =  struct('vx',blank,'vy',blank,'vz',blank,'n',blank,'nB',blank);
    end
end
clear blank

dims = 'xyz';
momu = {'Vx' 'Vy' 'Vz' 'dn' 'dnB'};
moml = {'vx' 'vy' 'vz' 'n' 'nB'};

% Read individual zone fields into full domain field variables
%%  read only n not vx, vy,vz, for k=1:4#
% i= is also to much, so only readiing some i's
BCadence=fcadence(1);
ECadence=fcadence(2);
nCadence=fcadence(3);

%nt=36000
%newCadence=4000
%laststr = 0;
bar=waitbar(0,'loading ...');
%msg_old = 'Reading_000%\n';
for p=1:nz
    waitbar(double(p)/double(nz))
    %for i=1:newCadence:(nt+1)
        % Display how far through the reading process we are
        %msg = strcat('Reading_',num2str(100*(i+(p-1)*nt)/(nz*nt),'%3.3i'),'%%\n');
        % This next bit deletes the old message to save space in the command window
        %if (~strcmp(msg,msg_old))
        %fprintf(repmat('\b',1,laststr));
        %fprintf(msg);
        %laststr = numel(msg)-2;
        %end
        %msg = msg_old;
        %waitbar(double(i)/double(nt+1))
        filename = strcat(fileprefix,'_z',num2str(p-1),'.hdf');
        fullpath = strcat(dest,filename);
        
        % Read zone field into temporary variable, then copy into the
        % larger field, missing out ghost cells
        for i=1:BCadence:(nt+1);
        for k = 1:3;
            if readB==true,
                temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/ZoneFields/B',dims(k)));
                B.(dims(k))(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/BCadence)) ...
                    = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
            end
        end
        end
        for i=1:ECadence:(nt+1);
        for k =1:3;
            if readE==true,
                temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/ZoneFields/E',dims(k)));
                E.(dims(k))(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/ECadence)) ...
                    = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
            end
        end % field loop
        end
        if readM==true;
            for i=1:nCadence:(nt+1);
            % Do the same for the moments, looping over each particle species
            for s=0:ns-1
                for k=1:5
                    temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/Pset/',num2str(s),'/ZoneMoments/',momu{k}));
                    M.(pname{s+1}).(moml{k})(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/nCadence)) ...
                        = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
                end % moment loop
            end % species loop
            end
        end
        
end % zone loop
%end
close(bar)
clear temp

%% 2D plot
t=50;
cellSize=dx(1);
cadence=BCadence;
%cellSize=0.465802;
%density =  squeeze(M.protons.nB(:,:,:,t)+M.test1.nB(:,:,:,t));
Bfield = squeeze(B.x(10,:,:,t));
%clims = [ 0.8 max(M.protons.nB(:)+M.test1.nB(:)) ]
%xx=(1:810)*cellSize;
%yy=(1:810)*cellSize;
xx=(1:nd(1))*cellSize*0.1111;
tt=(1:ceil(nt/cadence))*dt
imagesc(xx,tt,Bfield)
set(gca,'YDir','reverse');
ylabel('t /\Omega_i')
xlabel('x /\rho_i')
title('Bx')
%%
scrsz = get(0,'ScreenSize');
figure1 = figure('Position',[1 1  scrsz(3)*0.26 scrsz(4)*.638]);

% Create axes
axes1 = axes('Parent',figure1,'TickDir','in','PlotBoxAspectRatio',[1 1 1],...
    'LineWidth',2,...
    'FontWeight','bold',...    'xlim',[60.88 61.06],...[57.85 63.15],...    'ylim',[0 25],...     'YTick',0:10:20,...    'XTick',60.9:0.05:61.05,...    
    'FontSize',24);
% Uncomment the following line to preserve the X-limits of the axes
% xlim(axes1,[60.7 61]);
% Uncomment the following line to preserve the Y-limits of the axes
% ylim(axes1,[0 25]);
box(axes1,'on');
hold(axes1,'all');

%% Where is data

c           = 2.9979e10  ;%              cm / sec
kB          = 1.3807e-16 ;%            erg /deg(K)
eV          = 1.6022e-12 ;%  erg
q           = 4.8032e-10 ;% 1.6022e-19   statcoulomb
Bb           = 5e-8*1e4   ;%              gauss
n           = 50      ;%              cm^-3
m_p         = 1.6726e-24; %              g
m_e         = 9.1094e-28; %              g
w_pe        = sqrt((4*pi*n* q*q)/m_e);  %   1/secM4m4lluc4
w_pi        = sqrt((4*pi*n* q*q)/m_p);  %   1/sec
omega_e     = (q * Bb) / (m_e*c);        %   rad/sec
omega_i     = (q * Bb) / (m_p*c);        %   rad/sec
permeability= 1   ;                     % no units
mass_proton = 1.6726e-24 ;              %   g
V_a         = Bb/sqrt(permeability*n*mass_proton);
T_e         = 100 * eV ;          %(ratio  kB/ev) unit erg 
rho_e         = (sqrt(2*T_e/m_e)/omega_e)*1e-5; % km
rho_i         = (sqrt(2*T_e/m_p)/omega_i)*1e-5; % km
%% Create plot
if true
    YY=fft2(Bfield,1);
    imshow(log(abs(YY)))
    
end
if false
    w=linspace(0,2*pi*50,1000);
    k=sqrt( (w/c).^2 - (w*w_pe^2)./(c^2*(w-omega_e)) - (w*w_pi^2)./(c^2*(w+omega_i)));
    w=w/omega_i;
    k=2*pi*real(k)*(V_a/omega_i);
    
    plot(k,w,'Parent',axes1,'MarkerSize',12,'Marker','.','LineStyle',':',...
    'Color',[0 0 1],...
    'DisplayName','raw data')
    % line of w_pe and w
    array_w_pe=w_pe*(1:100);
    %plot(array_w_pe)
    %plot([0,max(k)+max(k)/20],[ omega_e/omega_i omega_e/omega_i], 'k-')
    plot([0,max(k)+max(k)/20],[ 1 1], 'k-')   
    % plot V_a
    t = k;
    hline1 = plot(t,t,'k');
% Create legend
    legend1 = legend(axes1,'show');
    set(legend1,...
    'Position',[0.604662077597002 0.817870632672333 0.0973091364205257 0.073300283286119]);
end
%ylabel('x ($\rho_i$)','interpreter', 'latex')
%axis([0 35.9964 0 35.9964]);
%%
% Generate figure and remove ticklabels


set(gca,'yticklabel',[], 'xticklabel', []) %Remove tick labels

% Get tick mark positions

yTicks = get(gca,'ytick');

xTicks = get(gca, 'xtick');

ax = axis; %Get left most x-position

HorizontalOffset = 0.1;

% Reset the ytick labels in desired font

for i = 1:length(yTicks)

%Create text box and set appropriate properties

     text(ax(1) - HorizontalOffset,yTicks(i),['$' num2str( yTicks(i)) '$'], ...
         'HorizontalAlignment','Right','interpreter', 'latex');   

end

% Reset the xtick labels in desired font 

minY = min(yTicks);

verticalOffset = 1.2;

for xx = 1:length(xTicks)

%Create text box and set appropriate properties

     text(xTicks(xx), minY - verticalOffset, ['$' num2str( xTicks(xx)) '$'], ...
         'HorizontalAlignment','Right','interpreter', 'latex');   

end
set(gca,'YDir','normal');

%% quick example
%h1=figure('Visible','off')
% For grid vectors x1gv, x2gv and x3gv of length M, N and P respectively
% meshgrid(x1gv, x2gv, x3gv) will output arrays of size N-by-M-by-P
figure()
[ x, y, z] = meshgrid(0:0.5:(nd(2)-0.5)*0.5,0:0.5:(nd(1)-0.5)*0.5,0:0.5:(nd(3)-0.5)*0.5);
xslice=[2,9]; 
yslice=[9]; 
zslice=[9];
t=1;
%v=M.test1.n(:,:,:,t);
%v=M.protons.n(:,:,:,t);

v=B.x(:,:,:,t);

 % V is an m-by-n-by-p  volume array containing data values at the default
 % location X = 1:n, Y = 1:m, Z = 1:p. Each element in the vectors sx, sy, 
 % and sz defines a slice plane in the x-, y-, or z-axis direction.
 % FIX: invert y,x,z slice
h=slice(x,y,z,v,yslice,xslice,zslice);
colormap hsv

%% investigate de data
cellSize=dx(1);
cs=cellSize;
[ x, y, z] = meshgrid((0:0.5:(nd(2)-0.5)*0.5)*cs,(0:0.5:(nd(1)-0.5)*0.5)*cs,(0:0.5:(nd(3)-0.5)*0.5)*cs);

t=51;
%nn=M.test1.n(:,:,:,t);
%n=M.protons.n(:,:,:,t);
%n=M.
v=B.x(:,:,:,t);

%v=n
%figure()

ymin = min(x(:)); 
xmin = min(y(:)); 
zmin = min(z(:));
nx=nd(1);
ny=nd(2);
nz=nd(3);
ymax = max(x(:)); 
xmax = max(y(:)); 
zmax = max(z(:));
alt=5/100*zmax; % raise the angle-plane
% Slice Plane at an Angle to the X-Axes
% surf(X,Y,Z) If X and Y are vectors, length(X) = n and length(Y)  = m,
% where [m,n] = size(Z)
hslice = surf(linspace(xmin,xmax,nx),...
	linspace(ymin,ymax,ny),...
        (zmax/2+alt)*ones(ny,nx));
	%((nx+ny)*cs*(0.5)/(2*2)+alt)*ones(ny,nx));
%
rotate(hslice,[0.5,0,0.],45)
xd = get(hslice,'XData');
yd = get(hslice,'YData');
zd = get(hslice,'ZData');
delete(hslice)
h=slice(x,y,z,v,yd,xd,zd)
set(h,'FaceColor','interp',...
	'EdgeColor','none',...
	'DiffuseStrength',.8)

%
hold on
yslice=[ymax]; 
hx = slice(x,y,z,v,yslice,[],[]);
set(hx,'FaceColor','interp','EdgeColor','none')


hy = slice(x,y,z,v,[],xmax,[]);
set(hy,'FaceColor','interp','EdgeColor','none')

hz = slice(x,y,z,v,[],[],zmin);
set(hz,'FaceColor','interp','EdgeColor','none')

% Define the View
%daspect([1,1,1])
axis tight
box on
view(-42,22)
camzoom(0.98)
camproj perspective
% Add Lighting and Specify Colors
lightangle(-48,15)
%colormap (jet(24))
set(gcf,'Renderer','zbuffer')

% Customize the Colormap
%colormap (flipud(jet(24)))
%colorbar('horiz')
colorbar('vertical')

 %caxis([min(M.protons.n(:)),max(M.test1.n(:))+max(M.protons.n(:))])
 caxis([min(v(:)),max(v(:))])
 %caxis([min(E.x(:)),max(E.x(:))])
 
 xlabel('y')
ylabel('x')
zlabel('z')
title(strcat(date(),' Blobs3D', ' t= ',num2str((t-1)*BCadence*dt),'\Omega_i^{-1}'),'FontSize',14)

%% fields plots
% use i=0 first row, i=1 second row, change time, use letter for B or E
cellSize=dx(1);
cs=cellSize;
[ x, y, z] = meshgrid((0:0.5:(nd(2)-0.5)*0.5)*cs,(0:0.5:(nd(1)-0.5)*0.5)*cs,(0:0.5:(nd(3)-0.5)*0.5)*cs);
i=1;   % first row is zero, second raw is 1
t=51;
strfield='E';
%nn=M.test1.n(:,:,:,t);
%n=M.protons.n(:,:,:,t);
%n=M.
if (strfield=='B')
vx=B.x(:,:,:,t);
vy=B.y(:,:,:,t);
vz=B.z(:,:,:,t);
elseif(strfield=='E')
vx=E.x(:,:,:,t);
vy=E.y(:,:,:,t);
vz=E.z(:,:,:,t);  
end
%v=n
%figure()

ymin = min(x(:)); 
xmin = min(y(:)); 
zmin = min(z(:));
nx=nd(1);
ny=nd(2);
nz=nd(3);
ymax = max(x(:)); 
xmax = max(y(:)); 
zmax = max(z(:));
alt=5/100*zmax; % raise the angle-plane
% Slice Plane at an Angle to the X-Axes
% surf(X,Y,Z) If X and Y are vectors, length(X) = n and length(Y)  = m,
% where [m,n] = size(Z)
hslice = surf(linspace(xmin,xmax,nx),...
	linspace(ymin,ymax,ny),...
        (zmax/2+alt)*ones(ny,nx));
	%((nx+ny)*cs*(0.5)/(2*2)+alt)*ones(ny,nx));
%
rotate(hslice,[0.5,0,0.],45)
xd = get(hslice,'XData');
yd = get(hslice,'YData');
zd = get(hslice,'ZData');
delete(hslice)
% vx
subplot(2,3,1+i*3)
h=slice(x,y,z,vx,yd,xd,zd)
set(h,'FaceColor','interp',...
	'EdgeColor','none',...
	'DiffuseStrength',.8)

%
hold on
yslice=[ymax]; 
hx = slice(x,y,z,vx,yslice,[],[]);
set(hx,'FaceColor','interp','EdgeColor','none')


hy = slice(x,y,z,vx,[],xmax,[]);
set(hy,'FaceColor','interp','EdgeColor','none')

hz = slice(x,y,z,vx,[],[],zmin);
set(hz,'FaceColor','interp','EdgeColor','none')

% Define the View
%daspect([1,1,1])
axis tight
box on
view(-42,22)
camzoom(0.98)
camproj perspective
% Add Lighting and Specify Colors
lightangle(-48,15)
%colormap (jet(24))
set(gcf,'Renderer','zbuffer')

% Customize the Colormap
%colormap (flipud(jet(24)))
%colorbar('horiz')
colorbar('vertical')

 %caxis([min(M.protons.n(:)),max(M.test1.n(:))+max(M.protons.n(:))])
 caxis([min(vx(:)),max(vx(:))])
 %caxis([min(E.x(:)),max(E.x(:))])
 
 xlabel('y')
ylabel('x')
zlabel('z')
title(strcat(date(),': ',strfield,'x', ' t= ',num2str((t-1)*BCadence*dt),'\Omega_i^{-1}'),'FontSize',14)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vy
subplot(2,3,2+i*3)
h=slice(x,y,z,vy,yd,xd,zd)
set(h,'FaceColor','interp',...
	'EdgeColor','none',...
	'DiffuseStrength',.8)

%
hold on
yslice=[ymax]; 
hx = slice(x,y,z,vy,yslice,[],[]);
set(hx,'FaceColor','interp','EdgeColor','none')


hy = slice(x,y,z,vy,[],xmax,[]);
set(hy,'FaceColor','interp','EdgeColor','none')

hz = slice(x,y,z,vy,[],[],zmin);
set(hz,'FaceColor','interp','EdgeColor','none')

% Define the View
%daspect([1,1,1])
axis tight
box on
view(-42,22)
camzoom(0.98)
camproj perspective
% Add Lighting and Specify Colors
lightangle(-48,15)
%colormap (jet(24))
set(gcf,'Renderer','zbuffer')

% Customize the Colormap
%colormap (flipud(jet(24)))
%colorbar('horiz')
colorbar('vertical')

 %caxis([min(M.protons.n(:)),max(M.test1.n(:))+max(M.protons.n(:))])
 caxis([min(vy(:)),max(vy(:))])
 %caxis([min(E.x(:)),max(E.x(:))])
 
 xlabel('y')
ylabel('x')
zlabel('z')
title(strcat(date(),': ',strfield,'y', ' t= ',num2str((t-1)*BCadence*dt),'\Omega_i^{-1}'),'FontSize',14)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vz
subplot(2,3,3+i*3)
h=slice(x,y,z,vz,yd,xd,zd)
set(h,'FaceColor','interp',...
	'EdgeColor','none',...
	'DiffuseStrength',.8)

%
hold on
yslice=[ymax]; 
hx = slice(x,y,z,vz,yslice,[],[]);
set(hx,'FaceColor','interp','EdgeColor','none')


hy = slice(x,y,z,vz,[],xmax,[]);
set(hy,'FaceColor','interp','EdgeColor','none')

hz = slice(x,y,z,vz,[],[],zmin);
set(hz,'FaceColor','interp','EdgeColor','none')

% Define the View
%daspect([1,1,1])
axis tight
box on
view(-42,22)
camzoom(0.98)
camproj perspective
% Add Lighting and Specify Colors
lightangle(-48,15)
%colormap (jet(24))
set(gcf,'Renderer','zbuffer')

% Customize the Colormap
%colormap (flipud(jet(24)))
%colorbar('horiz')
colorbar('vertical')

 %caxis([min(M.protons.n(:)),max(M.test1.n(:))+max(M.protons.n(:))])
 caxis([min(vz(:)),max(vz(:))])
 %caxis([min(E.x(:)),max(E.x(:))])
 
 xlabel('y')
ylabel('x')
zlabel('z')
title(strcat(date(),': ',strfield,'y', ' t= ',num2str((t-1)*BCadence*dt),'\Omega_i^{-1}'),'FontSize',14)

%% Adjust the Color Limits
% Not usually necessary
% caxis([0.7,1.8])
%set(h1,'Visible','on'); saveas(h1, 'UNI_3D_blob.png'); set(h1,'Visible','off');

%% density plot for 1D
figure()
   hold on
for i=1:20
    plot(v(i,:,16)+vv(i,:,20));
end
xlabel('x')
ylabel('Density along axis')
figure()
surf(linspace(ymin,ymax,ny),...
	linspace(xmin,xmax,nx),...
	v(:,:,1)+vv(:,:,1));
%% video
Bvideo=false;
Evideo=false;
nvideo=false;
name='N';
line='b';
%!!!!!!!!!!!!missing if's for differnt selection!!!!!!!!!!!!!!!!1
writerObj = VideoWriter(strcat(name,line,'.avi'));
open(writerObj);
%writerObj.FrameRate = 60;
%
[ x, y, z] = meshgrid(0:0.5:(nd(2)-0.5)*0.5,0:0.5:(nd(1)-0.5)*0.5,0:0.5:(nd(3)-0.5)*0.5);
for t=2:109
nn=M.test1.n(:,:,:,t);
n=M.protons.n(:,:,:,t);
%v=B.z(:,:,:,t);
colorBar=[min(M.protons.n(:)),max(M.test1.n(:))+max(M.protons.n(:))];
%colorBar=[min(B.z(:)),max(B.z(:))];
v=n+nn;
f1=figure();
set(f1,'position',[1640         400         600         500])
ymin = min(x(:)); 
xmin = min(y(:)); 
zmin = min(z(:));
nx=nd(1);
ny=nd(2);
nz=nd(3);
ymax = max(x(:)); 
xmax = max(y(:)); 
zmax = max(z(:));
alt=12/100*zmax; % raise the angle-plane
% Slice Plane at an Angle to the X-Axes
% surf(X,Y,Z) If X and Y are vectors, length(X) = n and length(Y)  = m,
% where [m,n] = size(Z)
hslice = surf(linspace(xmin,xmax,nx),...
	linspace(ymin,ymax,ny),...
	((nx+ny)*(0.5)/(2*2)+alt)*ones(ny,nx));
%
rotate(hslice,[0.5,0,0.],45)
xd = get(hslice,'XData');
yd = get(hslice,'YData');
zd = get(hslice,'ZData');
delete(hslice)
h=slice(x,y,z,v,yd,xd,zd);
set(h,'FaceColor','interp',...
	'EdgeColor','none',...
	'DiffuseStrength',.8)

%
hold on
yslice=[ymax]; 
hx = slice(x,y,z,v,yslice,[],[]);
set(hx,'FaceColor','interp','EdgeColor','none')


hy = slice(x,y,z,v,[],xmax,[]);
set(hy,'FaceColor','interp','EdgeColor','none')

hz = slice(x,y,z,v,[],[],zmin);
set(hz,'FaceColor','interp','EdgeColor','none')
% Define the View
%daspect([1,1,1])
axis tight
box on
view(-42,22)
camzoom(0.98)
camproj perspective
% Add Lighting and Specify Colors
lightangle(-48,15)
%colormap (jet(24))
set(gcf,'Renderer','zbuffer')

% Customize the Colormap
%colormap (flipud(jet(24)))
%colorbar('horiz')
colorbar('vertical');
 caxis(colorBar);
xlabel('y')
ylabel('x')
zlabel('z')
title('5NOV14 testii: Blobs3DIC (pmove, no field balance)','FontSize',14)

set(f1,'position',[1640         400         600         500])
   frame = getframe(f1);
   aa=size(frame.cdata);
   if aa(1)==500 && aa(2)==600,
       writeVideo(writerObj,frame);
   else
       display('fail')
   end
   close(gcf)
end
%
close(writerObj);
display('video Ready!')

%% Any video

name='B';
line='x';
%!!!!!!!!!!!!missing if's for differnt selection!!!!!!!!!!!!!!!!1
writerObj = VideoWriter(strcat(name,line,'.avi'));
open(writerObj);
%*************************************
%colorBar=[min(B.x(:)),max(B.x(:))];
colorBar=[0.98,1.0];
cellSize=0.04444;
xx=(1:810)*cellSize;
yy=(1:810)*cellSize;
%*************************************

for t=1:(nt/fcadence)
    f1=figure();
    %***************here paste **********
    %density =  squeeze(M.protons.nB(:,:,:,t)+M.test1.nB(:,:,:,t));
    Bfield = squeeze(B.x(:,:,:,t));
    %clims = [ 0.8 max(M.protons.nB(:)+M.test1.nB(:)) ]

    imagesc(xx,yy,Bfield)
    %set(gca,'YDir','reverse');
    colorbar('vertical');
    caxis(colorBar);
    title(strcat('Bx at t= ',t));
    %************************************
    set(f1,'position',[1640         400         600         500])
    frame = getframe(f1);
    aa=size(frame.cdata);
    if aa(1)==500 && aa(2)==600,
        writeVideo(writerObj,frame);
    else
        display('fail')
    end
    close(gcf)
end
%**************************
ylabel('y /\rho_i')
xlabel('x /\rho_i')
title('Bx')
%***************************
%
close(writerObj);
display('video Ready!')
