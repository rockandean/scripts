#PhD routines, def, scripts
import numpy as np
import matplotlib.pyplot as plt
#from mayavi.mlab import *
import h5py
from scipy import signal
from scipy.fftpack import fft,fftshift
class simParams:
    """
    sm=simParams(f)
    it include all simulation values
    """
    dt=0
    dxdydzcell=0
    electron_gamma=0
    hostzone=0
    initial_Te=0
    npsets=0
    substeps=0
    nxnynzdomain=0
    nzones=0
    prng_seed=0
    pscharge=0
    psname=0
    uniform_resis=0
    dxdydz=0
    nxnynz=0
    region=0
    xyz_domain=0
    zone=0
    #
    geometry=0
    nt=0
    fieldCadence=0
    def __init__(self,_f):
        simParams.dt             = _f['/SimulationParameters/dt'][0] # float
  #      simParams.nt             = _f['/SimulationParameters/nt'][0] # int
  #      simParams.fieldCadence   = _f['/SimulationParameters/fieldCadence'][0] # int 
        simParams.dxdydzcell     = _f['/SimulationParameters/dxdydzcell'] # array-like [:]
        simParams.electron_gamma = _f['/SimulationParameters/electron_gamma'][0] # float
        simParams.hostzone       = _f['/SimulationParameters/hostzone'][0] # float 
        simParams.initial_Te     = _f['/SimulationParameters/initial_Te'][0] # float 
        simParams.npsets         = _f['/SimulationParameters/npsets'][0] # float 
        simParams.nsubsteps      = _f['/SimulationParameters/nsubsteps'][0] # float 
        simParams.nxnynzdomain   = _f['/SimulationParameters/nxnynzdomain'] # array-like [:]
        simParams.nzones         = _f['/SimulationParameters/nzones'][0] # float 
        simParams.prng_seed      = _f['/SimulationParameters/prng_seed'][0] # float 
        simParams.pscharge       = _f['/SimulationParameters/pscharge'][0] # float 
        simParams.psname         = _f['/SimulationParameters/psname'][0]
        simParams.uniform_resis  = _f['/SimulationParameters/uniform_resis'][0] # float 
        simParams.dxdydz     = _f['/SimulationParameters/zinfo/dxdydz'] # array-like [:] 3
        simParams.nxnynz     = _f['/SimulationParameters/zinfo/nxnynz'] # array-like [:] 3
        simParams.region     = _f['/SimulationParameters/zinfo/region'] # array-like [:] 6
        simParams.xyz_domain = _f['/SimulationParameters/zinfo/xyz_domain'] # array-like [:] 6
        simParams.zone       = _f['/SimulationParameters/zinfo/zone'][0] # int this procese 
        simParams.geometry     = [int(self.xyz_domain[1]/self.region[1]),
                                int(self.xyz_domain[3]/self.region[3]),
                                int(self.xyz_domain[5]/self.region[5])]
    def getCellNumber(self):
        """
        nxnynzdomain = simParams.getCellNumber(f)
        """
        print ' x cell size domain:', self.nxnynzdomain[0]-1
        print ' y cell size domain:', self.nxnynzdomain[1]-1
        print ' z cell size domain:', self.nxnynzdomain[2]-1
        
    def readExtras(self,_nt,_fieldCadence):
        """
        simParams.readExtras(3600,3)
        """        
        simParams.nt            = _nt # int
        simParams.fieldCadence  = _fieldCadence # int
        
    def getField(self,_f,_fieldName,_axisName,axisLineA,axisLineB):
        """
        getField read a 2d array that contains a plane over time, for a given position [Xo,Yo,Zo].
        If uses axisName=X then Yo,Zo must be given or any other combination.
        Options are {Bx, By, Bz}, {Ex, Ey, Ez}. Use getCellNumber(f) to see the limits of
        each direction. Example:
            
        Bx=simParams.getField(f,'Bx','x',15, 15)    #where f is an HDF object
        """
        rootName=str(_f.filename)
        debbug = 0
        #
        if debbug:          

            _root = '/storage/space2/phrlaz/HybridCode/hypsi/data/17SEPT14/'
            _name = 'data_serial_z0.hdf'
            _f   = h5py.File(_root+_name,"r")
            rootName=str(_f.filename)        
            #_B=zeros((geometry[0]*nxnynz[0],_time.size))
            #_A=empty(_B.shape, dtype=complex)
            _fieldName = 'Bx'
            _axisName = 'x'
            axisLineA = 1
            axisLineB = 1


        ## time
        timeStep = _f['TimeStep']
        list     = [int(a) for a in timeStep.iterkeys()]
        list.sort()
        ## simulation parameters
        half=1
       # newCadence = self.fieldCadence
        newCadence = 3
        _time        = np.array(list[1:])[:int(np.size(list[1:])/half)]
        _time        = _time[::newCadence]
        cadence      = _time[1]-_time[0]
        # test if simParams match timeStep
        if _time.size!=self.nt:
            print 'nt mismatch, must run readExtras'
            simParams.nt = int(list[-1])        
        if cadence != self.fieldCadence:
            print 'field cadence mismatch, run readExtras'
            simParams.fieldCadence = cadence
            

        if np.array(self.geometry).prod()!=self.nzones:
            print('Error: Nzones and geometry do not match!')

        ## Pick axis: offset and axisLineA,axisLineB to avoid readings for too many hdf files
        if  _axisName == 'z':
            offset=1 # read up, no offset        
            _B=np.zeros((_time.size,self.geometry[2]*self.nxnynz[2])) #e.g.zeros(120,512)
            ax=np.array(range(self.nxnynzdomain[0])).reshape((self.geometry[0],self.nxnynz[0]))
            by=np.array(range(self.nxnynzdomain[1])).reshape((self.geometry[1],self.nxnynz[1]))
            #
            for i in range(self.geometry[0]):
                for j in range(self.geometry[1]):
                    # print 'ERROR find first hdf file'
                    if (ax[i,:] == axisLineA).any() & (by[j,:] == axisLineB).any():
                        # any test for any True
                        cont = i*self.geometry[1]*self.geometry[2]+j*self.geometry[2]
                        print 'Position:', j ,i,' Starting node: ', cont 
                        axisLineA_=axisLineA-i*self.nxnynz[0]
                        axisLineB_=axisLineB-j*self.nxnynz[1]
                        
            print 'Starting node: ', cont             
            for k in range(self.geometry[2]):
                print 'Loop: ', k+1, '/', self.geometry[2]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time:
                    _B[int((t-1)/cadence),k*self.nxnynz[2]:self.nxnynz[2]*(k+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][axisLineA_,axisLineB_,1:-1]
                cont+=offset
            #cont=cont2   # if ever change time loop again out  
            
        elif _axisName == 'y':
            offset=self.geometry[2] # only z is under y
            _B=np.zeros((_time.size,self.geometry[1]*self.nxnynz[1])) #e.g.zeros(120,512)
            ax=np.array(range(self.nxnynzdomain[0])).reshape((self.geometry[0],self.nxnynz[0]))
            bz=np.array(range(self.nxnynzdomain[2])).reshape((self.geometry[2],self.nxnynz[2]))
            #
            for i in range(self.geometry[0]):
                for k in range(self.geometry[2]):
                    #if cont > offset:
                    # print 'ERROR find first hdf file'
                    if (ax[i,:] == axisLineA).any() &  (bz[k,:] == axisLineB).any():
                        cont=k+i*self.geometry[1]*self.geometry[2]#contAux
                        print 'Position:', k ,i
                        axisLineA_=axisLineA-i*self.nxnynz[0]
                        axisLineB_=axisLineB-k*self.nxnynz[2]
    
            print 'Starting node: ', cont 
            for j in range(self.geometry[1]):
                print j
                print 'Loop: ', j+1, '/', self.geometry[1]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time:
                    _B[int((t-1)/cadence),j*self.nxnynz[1]:self.nxnynz[1]*(j+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][axisLineA_,1:-1,axisLineB_]
                cont+=offset
            #cont=cont2   # if ever change time loop again out 
                
        elif _axisName == 'x':
            offset=self.geometry[1]*self.geometry[2]
            _B=np.zeros((_time.size,self.geometry[0]*self.nxnynz[0])) #e.g.zeros(120,512)
            ay=np.array(range(self.nxnynzdomain[1])).reshape((self.geometry[1],self.nxnynz[1]))
            bz=np.array(range(self.nxnynzdomain[2])).reshape((self.geometry[2],self.nxnynz[2]))
            #
            for j in range(self.geometry[1]):
                for k in range(self.geometry[2]):
                    #if cont > offset:
                    # print 'ERROR find first hdf file'
                    if (ay[j,:] == axisLineA).any() &  (bz[k,:] == axisLineB).any():
                        cont=j*self.geometry[2]+k
                        print 'Position:', k ,j
                        axisLineA_=axisLineA-j*self.nxnynz[1]
                        axisLineB_=axisLineB-k*self.nxnynz[2]
    
            print 'Starting node: ', cont
            for i in range(self.geometry[0]):
                print 'Loop: ', i+1, '/', self.geometry[0]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time: 
                        _B[int((t-1)/cadence),i*self.nxnynz[0]:self.nxnynz[0]*(i+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][1:-1,axisLineA_,axisLineB_]
                cont+=offset
            #cont=cont2   # if ever change time loop again out
        else:
            print 'Error to read field direction'
        #    
        return _B
        
    def FFT(self,_f,_B,_axisName,axisLineA,axisLineB):
        """
        FFT(_f,_B,_axisName,axisLineA,axisLineB)
        """
        #
        debbug = 0
        if debbug:          
            root = '/storage/space2/phrlaz/HybridCode/hypsi/data/'
            name = 'data_disp1d2x2_z0.hdf'
            _f   = h5py.File(root+name,"r")
            rootName=str(_f.filename)        
            #_B=zeros((geometry[0]*nxnynz[0],_time.size))
            #_A=empty(_B.shape, dtype=complex)
            _fieldName = 'By'
            _axisName='x'
            axisLineA = 1
            axisLineB = 1
            self = simParams(f)
            _B=self.getField(_f,_fieldName,_axisName,axisLineA, axisLineB)
        #     
        if _axisName =='x':
            cell=0
        elif _axisName =='y':
            cell=1
        elif _axisName =='z':
            cell=2
        else:
            print 'Error reading cell size, FFT()'
        
        # time dt,T box size dx,L
        Nt =self.nt/self.fieldCadence
        Nx =self.nxnynzdomain[cell]
        _dt     = self.dt*self.fieldCadence
        _Tsize  = _dt*Nt  
        _dx     = self.dxdydzcell[cell]
        _Lsize  = _dx*Nx
        
        # main figure
        plt.figure()
        _y=np.empty(_B.shape, dtype=complex) 
        y=np.empty(_B.shape, dtype=complex)
        plt.subplot(311)
        
        # range of plot
        kmin=-np.pi/(_dx) ##- 2*pi/ 2*(dx)
        kmax=np.pi/(_dx)
        krange=np.r_[kmin:kmax:2*np.pi/(_dx*Nx)]
        
        wmin=-np.pi/(_dt)
        wmax=np.pi/(_dt)
        wrange=np.r_[wmin:wmax:2*np.pi/(_dt*Nt)]

        # field
        im=plt.imshow(_B,aspect='auto',interpolation='none',origin='lower',extent=[0,_Lsize,0,_Tsize]) #,origin='lower'
        plt.colorbar(im,orientation='vertical')
        plt.title(' cell number line A='+str(axisLineA)+', B='+str(axisLineB))
        plt.xlabel('position $(d_i)$')
        plt.ylabel('time $(\Omega_i)$')
        
        # lines, velocities
        plt.plot()
        plt.title(' cell number line A='+str(axisLineA)+', B='+str(axisLineB))
        plt.xlabel('position $(d_i)$')
        plt.ylabel('time $(\Omega_i)$')
        
        # FFT
        plt.subplot(312)
        win=signal.hann(_B.shape[0])
        win2=signal.hann(_B.shape[1])
        for t in range(_B.shape[0]):
            _y[t,:]=fftshift(fft(win2*_B[t,:]))/_B.shape[1]
        for x in range(_B.shape[1]):
            y[:,x] = fftshift(fft(win*_y[:,x]))/_B.shape[0]
        Y=y*y.conjugate()
        PSK=(Y.real)
        #
        im=plt.imshow(np.log(PSK),aspect='auto',interpolation='none',extent=[kmin,kmax,wmin,wmax])
        plt.colorbar(im,orientation='vertical')
        plt.xlim([-kmax/1000, kmax])
        plt.ylim([-wmax/1000, wmax])    
        plt.title('')
        plt.xlabel('wavenumber $k$ $(rad d_i^{-1})$')
        plt.ylabel('frequency $(rad \omega_i )$')
        #line alfven wave
        t = np.arange(0,krange.max())
        plt.plot(t,'k-')
        plt.plot(t,2*np.pi*np.ones(t.size),'k--')
        
        plt.subplot(313)
        im=plt.imshow(np.log(PSK),aspect='auto',interpolation='none',extent=[kmin,kmax,wmin,wmax])
        plt.colorbar(im,orientation='vertical')     
        plt.title('')
        plt.xlabel('wavenumber $k$ $(rad d_i^{-1})$')
        plt.ylabel('frequency $(rad \omega_i )$')    

    
def zinfo(_f):
    """
    dxdydz, nxnynz, region, xyz_domain, zone = zinfo(f)
    """
    _dxdydz     = _f['/SimulationParameters/zinfo/dxdydz'] # array-like [:] 3
    _nxnynz     = _f['/SimulationParameters/zinfo/nxnynz'] # array-like [:] 3
    _region     = _f['/SimulationParameters/zinfo/region'] # array-like [:] 6
    _xyz_domain = _f['/SimulationParameters/zinfo/xyz_domain'] # array-like [:] 6
    _zone       = _f['/SimulationParameters/zinfo/zone'][0] # int this procese 
    return _dxdydz, _nxnynz, _region, _xyz_domain, _zone
    
def openHDF(_name):#,N=None):
    #import h5py
    """
    Open the file _name so we can read from it.
    root='/storage/space2/phrlaz/HybridCode/hypsi/data/21MAY14/'
    name='input_file_2_z0.hdf'
    f=openHDF(root+name)
    f=openDHF('31Jan2014/data_speed_output_z0.hdf',3) for the *z3.hdf files
    """
    #open output_hdf
    #if N is not None:
     #   N_ = N
    #else:
     #   N_= 0
    #name_ = '31Jan2014/data_speed_output_z0.hdf'
    #f_ = h5py.File(_name[:-5]+str(N_)+'.hdf','r')
    _f = h5py.File(_name,'r')
    return _f



    
    #plt.subplot(313)
    #im=plt.imshow(PSK,aspect='auto',interpolation='none',origin='lower')
    #plt.colorbar(im,orientation='vertical')
    #plt.xlim([-fmax, fmax])    
def getDomain(_geometry):
    """
    getDomain([5.0,4.0,3.0])
    get a domain {60,40,40}, that conmensurate with node geometry
    """
    debbug=0
    if debbug:
        _geometry=[6.0,5.0,4.0]
    for i in range(1,21):
        print i, "{ "+str(i*_geometry[0])+", "+str(i*_geometry[1])+", "+str(i*_geometry[2])+"}"
        print " Don't forget to reallocate particles! "

def plotParticles(_f,thisParticle):
    """
    plotParticel(f, 42)
    """
    #
    debbug = 1
    if debbug:          
        _root = '/storage/space2/phrlaz/HybridCode/hypsi/data/13AUG14/c/'
        _name = 'data_serial_z0.hdf'
        _f   = h5py.File(root+name,"r")
        rootName=str(_f.filename)
        thisParticle=424
             
    # time 
    _particleData=_f['ParticleData']
    list = [ int(a[9:]) for a in _particleData.iterkeys()]
    list.sort()
    
    ## simulation parameters
    half=1
    _time = np.array(list[1:])[:int(np.size(list[1:])/half)]    
    cadence  = _time[1]-_time[0]     
    #print _time    
    #position
    npdata = _f['ParticleData/timestep_0/sample_0/pset_0/npdata']
    #pdata  = [ _f['ParticleData/timestep_'+str(a)+'/sample_0/pset_0/pdata'][thisParticle] for a in _time ]
    _xv=np.zeros((_time.size,6)) 
    for t in _time:
        _xv[int((t-1)/cadence),:]= _f['ParticleData/timestep_'+str(t)+'/sample_0/pset_0/pdata'][thisParticle]
    

    
    x = _xv[:,0]
    y = _xv[:,1]
    z = _xv[:,2]
    

    
    plt.plot(x,z,'.')
    plt.plot(y,z,'.r')
    plt.plot(x,y,'.g')
    #plot3d(x, y, z, tube_radius=0.025,  colormap='Spectral')#
    #t=64
    plt.figure()

    for t in range(0,1600,500):
        _v= _f['ParticleData/timestep_'+str(t)+'/sample_0/pset_0/pdata']
        mys=['x position', 
            'y position',
            'z position',
            'vx',
            'vy',
            'vz']
    #plt.figure()
        for i in range(6):
            subplot(2,3,i+1)
            A,B=histogram(_v[:,i],50)#,density=True)
            plt.plot(B[:-1],A)
            plt.xlabel(mys[i])
    
