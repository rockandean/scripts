#!/home/space/phrlaz/Enthought/Canopy_64bit/System/bin/python
##!/usr/bin/python
# plot uniform plasma with background field
# Load libraries
import numpy as np
import matplotlib.pyplot as plt
#from mayavi.mlab import *
import h5py
from scipy import signal
from scipy.fftpack import fft,fftshift

# my routines
#from phd import *
#%%
class simParams:
    """
    sm=simParams(f)
    it include all simulation values
    """
    dt=0
    dxdydzcell=0
    electron_gamma=0
    hostzone=0
    initial_Te=0
    npsets=0
    substeps=0
    nxnynzdomain=0
    nzones=0
    prng_seed=0
    pscharge=0
    psname=0
    uniform_resis=0
    dxdydz=0
    nxnynz=0
    region=0
    xyz_domain=0
    zone=0
    #
    geometry=0
    nt=0
    fieldCadence=0
    def __init__(self,_f):
        simParams.dt             = _f['/SimulationParameters/dt'][0] # float
        try:
            simParams.nt             = _f['/SimulationParameters/nt'][0] # int
            simParams.fieldCadence   = _f['/SimulationParameters/fieldCadence'][0] # int 
        except:
            pass
        simParams.dxdydzcell     = _f['/SimulationParameters/dxdydzcell'] # array-like [:]
        simParams.electron_gamma = _f['/SimulationParameters/electron_gamma'][0] # float
        simParams.hostzone       = _f['/SimulationParameters/hostzone'][0] # float 
        simParams.initial_Te     = _f['/SimulationParameters/initial_Te'][0] # float 
        simParams.npsets         = _f['/SimulationParameters/npsets'][0] # float 
        simParams.nsubsteps      = _f['/SimulationParameters/nsubsteps'][0] # float 
        simParams.nxnynzdomain   = _f['/SimulationParameters/nxnynzdomain'] # array-like [:]
        simParams.nzones         = _f['/SimulationParameters/nzones'][0] # float 
        simParams.prng_seed      = _f['/SimulationParameters/prng_seed'][0] # float 
        simParams.pscharge       = _f['/SimulationParameters/pscharge'][0] # float 
        #simParams.psname         = _f['/SimulationParameters/psname'][0]
        simParams.uniform_resis  = _f['/SimulationParameters/uniform_resis'][0] # float 
        simParams.dxdydz     = _f['/SimulationParameters/zinfo/dxdydz'] # array-like [:] 3
        simParams.nxnynz     = _f['/SimulationParameters/zinfo/nxnynz'] # array-like [:] 3
        simParams.region     = _f['/SimulationParameters/zinfo/region'] # array-like [:] 6
        simParams.xyz_domain = _f['/SimulationParameters/zinfo/xyz_domain'] # array-like [:] 6
        simParams.zone       = _f['/SimulationParameters/zinfo/zone'][0] # int this procese 
        simParams.geometry     = [int(self.xyz_domain[1]/self.region[1]),
                                int(self.xyz_domain[3]/self.region[3]),
                                int(self.xyz_domain[5]/self.region[5])]
    def getCellNumber(self):
        """
        nxnynzdomain = simParams.getCellNumber(f)
        """
        print ' x cell size domain:', self.nxnynzdomain[0]-1
        print ' y cell size domain:', self.nxnynzdomain[1]-1
        print ' z cell size domain:', self.nxnynzdomain[2]-1
        
    def readExtras(self,_nt,_fieldCadence):
        """
        simParams.readExtras(3600,3)
        """        
        self.nt            = _nt # int
        self.fieldCadence  = _fieldCadence # int
        
    def getField(self,_f,_fieldName,_axisName,axisLineA,axisLineB):
        """
        getField read a 2d array that contains a plane over time, for a given position [Xo,Yo,Zo].
        If uses axisName=X then Yo,Zo must be given or any other combination.
        Options are {Bx, By, Bz}, {Ex, Ey, Ez}. Use getCellNumber(f) to see the limits of
        each direction. Example:
            
        Bx=simParams.getField(f,'Bx','x',15, 15)    #where f is an HDF object
        """
        rootName=str(_f.filename)
        debbug = 0
        #
        if debbug:          

            _root='/storage/space2/phrlaz/HybridCode/hypsi/data/d2015/JAN/27JAN15/'
            _name='data_test_z0.hdf'
            _f   = h5py.File(_root+_name,"r")
            rootName=str(_f.filename)
            self=simParams(f)
            #_B=zeros((geometry[0]*nxnynz[0],_time.size))
            #_A=empty(_B.shape, dtype=complex)
            _fieldName = 'Bx'
            _axisName = ['y',_fieldName]
            axisLineA = 0
            axisLineB = 1


        ## time
        timeStep = _f['TimeStep']
        list     = [int(a) for a in timeStep.iterkeys()]
        list.sort()
        ## simulation parameters
        half=1
        #newCadence = self.fieldCadence
        #newCadence = 3
        _time        = np.array(list[1:])[:int(np.size(list[1:])/half)]
        #_time        = _time[::newCadence]
        cadence      = _time[1]-_time[0]
        # test if simParams match timeStep
        if _time.size!=self.nt/self.fieldCadence:
            print 'nt mismatch, must run readExtras'
            self.nt = int(list[-1])        
        if cadence != self.fieldCadence:
            print 'field cadence mismatch, run readExtras'
            cadence = self.fieldCadence
            

        if np.array(self.geometry).prod()!=self.nzones:
            print('Error: Nzones and geometry do not match!')

        ## Pick axis: offset and axisLineA,axisLineB to avoid readings for too many hdf files
        if  _axisName[0] == 'z':
            offset=1 # read up, no offset        
            _B=np.zeros((_time.size,self.geometry[2]*self.nxnynz[2])) #e.g.zeros(120,512)
            ax=np.array(range(self.nxnynzdomain[0])).reshape((self.geometry[0],self.nxnynz[0]))
            by=np.array(range(self.nxnynzdomain[1])).reshape((self.geometry[1],self.nxnynz[1]))
            #
            for i in range(self.geometry[0]):
                for j in range(self.geometry[1]):
                    # print 'ERROR find first hdf file'
                    if (ax[i,:] == axisLineA).any() & (by[j,:] == axisLineB).any():
                        # any test for any True
                        cont = i*self.geometry[1]*self.geometry[2]+j*self.geometry[2]
                        print 'Position:', j ,i,' Starting node: ', cont 
                        axisLineA_=axisLineA-i*self.nxnynz[0]
                        axisLineB_=axisLineB-j*self.nxnynz[1]
                        
            print 'Starting node: ', cont             
            for k in range(self.geometry[2]):
                print 'Loop: ', k+1, '/', self.geometry[2]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time:
                    _B[int((t-1)/cadence),k*self.nxnynz[2]:self.nxnynz[2]*(k+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][axisLineA_,axisLineB_,1:-1]
                cont+=offset
            #cont=cont2   # if ever change time loop again out  
            
        elif _axisName[0] == 'y':
            offset=self.geometry[2] # only z is under y
            _B=np.zeros((_time.size,self.geometry[1]*self.nxnynz[1])) #e.g.zeros(120,512)
            ax=np.array(range(self.nxnynzdomain[0])).reshape((self.geometry[0],self.nxnynz[0]))
            bz=np.array(range(self.nxnynzdomain[2])).reshape((self.geometry[2],self.nxnynz[2]))
            #
            for i in range(self.geometry[0]):
                for k in range(self.geometry[2]):
                    #if cont > offset:
                    # print 'ERROR find first hdf file'
                    if (ax[i,:] == axisLineA).any() &  (bz[k,:] == axisLineB).any():
                        cont=k+i*self.geometry[1]*self.geometry[2]#contAux
                        print 'Position:', k ,i
                        axisLineA_=axisLineA-i*self.nxnynz[0]
                        axisLineB_=axisLineB-k*self.nxnynz[2]
    
            print 'Starting node: ', cont 
            for j in range(self.geometry[1]):
                print j
                print 'Loop: ', j+1, '/', self.geometry[1]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time:
                    _B[int((t-1)/cadence),j*self.nxnynz[1]:self.nxnynz[1]*(j+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][axisLineA_,1:-1,axisLineB_]
                cont+=offset
            #cont=cont2   # if ever change time loop again out 
                
        elif _axisName[0] == 'x':
            offset=self.geometry[1]*self.geometry[2]
            _B=np.zeros((_time.size,self.geometry[0]*self.nxnynz[0])) #e.g.zeros(120,512)
            ay=np.array(range(self.nxnynzdomain[1])).reshape((self.geometry[1],self.nxnynz[1]))
            bz=np.array(range(self.nxnynzdomain[2])).reshape((self.geometry[2],self.nxnynz[2]))
            #
            for j in range(self.geometry[1]):
                for k in range(self.geometry[2]):
                    #if cont > offset:
                    # print 'ERROR find first hdf file'
                    if (ay[j,:] == axisLineA).any() &  (bz[k,:] == axisLineB).any():
                        cont=j*self.geometry[2]+k
                        print 'Position:', k ,j
                        axisLineA_=axisLineA-j*self.nxnynz[1]
                        axisLineB_=axisLineB-k*self.nxnynz[2]
    
            print 'Starting node: ', cont
            for i in range(self.geometry[0]):
                print 'Loop: ', i+1, '/', self.geometry[0]
                _f=h5py.File(rootName[:-5]+str(cont)+'.hdf',"r")
                print 'hostZone:'+str(_f['/SimulationParameters/hostzone'][0])
                for t in _time: 
                        _B[int((t-1)/cadence),i*self.nxnynz[0]:self.nxnynz[0]*(i+1)]= _f['/TimeStep/'+str(t)+'/ZoneFields/'+_fieldName][1:-1,axisLineA_,axisLineB_]
                cont+=offset
            #cont=cont2   # if ever change time loop again out
        else:
            print 'Error to read field direction'
        #    
        return _B 
        
    def FFT(self,_f,_B,_axisName,axisLineA,axisLineB):
        """
        FFT(_f,_B,_axisName,axisLineA,axisLineB)
        """
        #
        debbug = 0
        if debbug:          
            root = '/storage/space2/phrlaz/HybridCode/hypsi/data/'
            name = 'data_disp1d2x2_z0.hdf'
            _f   = h5py.File(root+name,"r")
            rootName=str(_f.filename)        
            #_B=zeros((geometry[0]*nxnynz[0],_time.size))
            #_A=empty(_B.shape, dtype=complex)
            _fieldName = 'By'
            _axisName=['x',_fieldName]
            axisLineA = 1
            axisLineB = 1
            self = simParams(f)
            _B=self.getField(_f,_fieldName,_axisName[0],axisLineA, axisLineB)
        #     
        if _axisName[0] =='x':
            cell=0
            _Aline='Y'
            _Bline='Z'
        elif _axisName[0] =='y':
            cell=1
            _Aline='X'
            _Bline='Z'
        elif _axisName[0] =='z':
            cell=2
            _Aline='X'
            _Bline='Y'
        else:
            print 'Error reading cell size, FFT()'
        
        # time dt,T box size dx,L
        Nt =self.nt/self.fieldCadence
        Nx =self.nxnynzdomain[cell]
        _dt     = self.dt*self.fieldCadence
        _Tsize  = _dt*Nt  
        _dx     = self.dxdydzcell[cell]
        _Lsize  = _dx*Nx
        
        # main figure
        plt.figure()
        _y=np.empty(_B.shape, dtype=complex) 
        y=np.empty(_B.shape, dtype=complex)
        plt.subplot(311)
        
        # range of plot
        kmin=-np.pi/(_dx) ##- 2*pi/ 2*(dx)
        kmax=np.pi/(_dx)
        krange=np.r_[kmin:kmax:2*np.pi/(_dx*Nx)]
        
        wmin=-np.pi/(_dt)
        wmax=np.pi/(_dt)
        
        wrange=np.r_[wmin:wmax:1/(_dt*Nt)] #because 2Pi is included in units.

        # field
        im=plt.imshow(_B,aspect='auto',interpolation='none',origin='lower',extent=[0,_Lsize,0,_Tsize]) #,origin='lower'
        plt.colorbar(im,orientation='vertical')
        plt.title(_axisName[1] + ' over the plane' +_Aline+'= '+str(axisLineA)+', '+_Bline+'= '+str(axisLineB))
        plt.xlabel('$position/(d_i)$')
        plt.ylabel('$(time*\Omega_i)$')
        
        # FFT
        plt.subplot(312)
        win=signal.hann(_B.shape[0])
        win2=signal.hann(_B.shape[1])
        for t in range(_B.shape[0]):
            _y[t,:]=fftshift(fft(win2*_B[t,:]))/_B.shape[1]
        for x in range(_B.shape[1]):
            y[:,x] = fftshift(fft(win*_y[:,x]))/_B.shape[0]
        Y=y*y.conjugate()
        PSK=(Y.real)
        #
        im=plt.imshow(np.log(PSK),aspect='auto',interpolation='none',extent=[kmin,kmax,wmin,wmax])
        plt.colorbar(im,orientation='vertical')
        plt.xlim([-kmax/1000, kmax])
        plt.ylim([-wmax/1000, wmax])    
        plt.title('')
        plt.xlabel('$k (d_i)$')
        plt.ylabel('$w (\Omega_i^{-1} )$')
        #line alfven wave
        t = np.arange(0,krange.max())
        plt.plot(t,'k-')
        plt.plot(t,np.ones(t.size),'k--')
        
        plt.subplot(313)
        im=plt.imshow(np.log(PSK),aspect='auto',interpolation='none',extent=[kmin,kmax,wmin,wmax])
        plt.colorbar(im,orientation='vertical')     
        plt.title('')
        plt.xlabel('$k\ (d_i)$')
        plt.ylabel('$w\ (\Omega_i^{-1} )$')
        
    def plotParticles(_f,tend,thisParticle):
        """
        This function Assumes Bfield is in x-direction.
        for t in range(0,5):
            plotParticles(f,180,37000+1000*t)
        """
        #
        debbug = 0
        if debbug:    
            _root='/storage/space2/phrlaz/gitRepos/benhypsi/hypsi/'
            _name='data_testUniform_z0.hdf'
            _f   = h5py.File(_root+_name,"r")
            rootName=str(_f.filename)
            thisParticle=35005
            tend =200
                 
        # time 
        _particleData=_f['ParticleData']
        list = [ int(a[9:]) for a in _particleData.iterkeys()]
        list.sort()
        
        ## simulation parameters
        half=1
        _time = np.array(list[:tend])[:int(np.size(list[:tend])/half)]    
        cadence  = _time[1]-_time[0]     
        #print _time    
        #position
        npdata = _f['ParticleData/timestep_0/sample_0/pset_0/npdata'][0]
        xyz_domain = _f['/SimulationParameters/zinfo/xyz_domain'] # array-like [:] 6
        dxdydz = _f['/SimulationParameters/zinfo/dxdydz'] # array-like [:] 6
        #pdata  = [ _f['ParticleData/timestep_'+str(a)+'/sample_0/pset_0/pdata'][thisParticle] for a in _time ]
        _xv=np.zeros((_time.size,6))
        for t in _time:
            print t
            _xv[int((t)/cadence),:]= _f['ParticleData/timestep_'+str(t)+'/sample_0/pset_0/pdata'][thisParticle]
    #
        x = _xv[:,0]
        y = _xv[:,1]
        z = _xv[:,2]
        #plt.plot(_time,z,'o-g')
        #plt.plot(x,z,'.')
        #tend=20
    
        #plt.plot(y[:tend]-y[:tend].mean(),z[:tend]-z[:tend].mean(),'.-k')
        plt.plot(y[:tend],z[:tend],'.-')
        plt.axis((xyz_domain[2],xyz_domain[3],xyz_domain[4],xyz_domain[5]))
        plt.grid(True)
        plt.xticks([i for i in np.arange(0,xyz_domain[3],dxdydz[1])])
        plt.yticks([i for i in np.arange(0,xyz_domain[5],dxdydz[2])])
        #plt.axis([0,20,0,20])
        #plt.gca().set_xticks(np.arange(0,20,0.5))
        #plt.gca().set_yticks(np.arange(0,20,0.5))    
        #plt.gca().grid(True)
        plt.xlabel('$r_gi/\Omega_i$')
        plt.ylabel('$r_gi/\Omega_i$')
        plt.title('B=0.1 ; nt=200 ; dx=0.1111 ;dt=0.1 ; vth=0.09')
                 
#%% data parallel
#root='/storage/space2/phrlaz/HybridCode/hypsi/data/'
#name='data_disp1d2x2_z0.hdf'
root='/storage/space2/phrlaz/HybridCode/hypsi/data/d2015/AUG/03/'
name='data_Blobs3D_z0.hdf'
f=h5py.File(root+name,'r')

#root='/storage/space2/phrlaz/gitRepos/benhypsi/hypsi/'
#name='data_blob2d_z0.hdf'
#name='data_testUniform_z0.hdf'
#f=h5py.File(root+name,'r')


#%% check number of cell per axis
sm = simParams(f)

#read missing nt and field cadence
#sm.readExtras(39600,120)

#% print cell number
sm.getCellNumber()

#%%
aux='B'
field=aux+'x'
axis='z'  # line of sight: build line in this axis
axisA=0
axisB=31
# read field 
B=sm.getField(f,field,axis,axisA,axisB)
#%%
#  plot FFT
sm.FFT(f,B,[axis,field],axisA,axisB)
plt.show()
#%
field=aux+'y'
#axis='x'
axisA=0
axisB=10
# read field 
B=sm.getField(f,field,axis,axisA,axisB)

# plot FFT
sm.FFT(f,B,[axis,field],axisA,axisB)
plt.show()
#
field=aux+'z'
#axis='x'
axisA=0
axisB=40
# read field 
B=sm.getField(f,field,axis,axisA,axisB)

# plot FFT
sm.FFT(f,B,[axis,field],axisA,axisB)
plt.show()
#%%
#plt.close('all')
