 % this is to plot the 3d plot of the blob in t=0
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% http://www.mathworks.co.uk/help/matlab/ref/slice.html
% http://www.mathworks.co.uk/help/matlab/visualize/exploring-volumes-with-slice-planes.html
%
% HYPSI field file stitcher
%
% Script combines field dumps for several processors (e.g.
% data_bench_z0-9.hdf) into field variables such as B.x, E.x, M.protons.vx
% etc.
%
% Written by P W Gingell
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
% Data file locations
clear all
dest = '/storage/space2/phrlaz/HybridCode/hypsi/data/d2015/AUG/06/';
%dest = '/storage/space2/phrlaz/gitRepos/benhypsi/hypsi/run/'; % Data file path
fileprefix = 'data_Blobs3D'; % Data file root name

readM = true; % Particle moments n, v

% Field dump cadence (timesteps between data dumps)
% Note: currently assumes the same cadence for all dumped fields

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Read file zero for domain parameters
filename = strcat(fileprefix,'_z0.hdf');
fullpath = strcat(dest,filename);
ns = h5read(fullpath,'/SimulationParameters/npsets');
psname= h5read(fullpath,'/SimulationParameters/psname');
%ns = 1 %// trick to read matrix in    
nz = h5read(fullpath,'/SimulationParameters/nzones');    
dx = h5read(fullpath,'/SimulationParameters/zinfo/dxdydz');
nd = double(h5read(fullpath,'/SimulationParameters/nxnynzdomain'));
ts_info = h5info(fullpath,'/TimeStep');
nt = max(str2num(strrep(strcat(ts_info.Groups.Name),'/TimeStep/',' ')));
%nt = cast(h5read(fullpath,'/SimulationParameters/nt'),'double');
%nt=cast(5300,'double');
dt = h5read(fullpath,'/SimulationParameters/dt');
fcadence =cast( h5read(fullpath,'/SimulationParameters/fieldCadence'),'double');

BCadence=fcadence(1);
ECadence=fcadence(2);
nCadence=fcadence(3);

region = zeros(nz,6);
region(1,:) = h5read(fullpath,'/SimulationParameters/zinfo/region');
%
% Read all other zone files for region information
for p=1:nz-1;
    filename = strcat(fileprefix,'_z',num2str(p),'.hdf');
    fullpath = strcat(dest,filename);
    region(p+1,:) = h5read(fullpath,'/SimulationParameters/zinfo/region');   
end

% Convert region data to processor cell limits
rcell = int32([region(:,1)/dx(1)+1 ...
         region(:,2)/dx(1)   ...
         region(:,3)/dx(2)+1 ...
         region(:,4)/dx(2)   ...
         region(:,5)/dx(3)+1 ...
         region(:,6)/dx(3)]);

rcell=double(rcell);
clear region

% Initialise moment variables for full simulation domain
if readM==true,
        % Find the name of each species for use in the particle moment
        % struct. For example: M.(pname).vx
        pname=strsplit(psname,',');
end

%
if readM==true,
    blank = zeros(nd(1),nd(2),nd(3),ceil(nt/nCadence));
    for p=0:ns-1
        %p=0
        %pname ={'proton'};
        M.(pname{p+1}) =  struct('vx',blank,'vy',blank,'vz',blank,'n',blank,'nB',blank);
    end
end
clear blank

dims = 'xyz';
momu = {'Vx' 'Vy' 'Vz' 'dn' 'dnB'};
moml = {'vx' 'vy' 'vz' 'n' 'nB'};

%% %%  read only n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bar=waitbar(0,'loading ...');
%msg_old = 'Reading_000%\n';
for p=1:nz
    waitbar(double(p)/double(nz))
        filename = strcat(fileprefix,'_z',num2str(p-1),'.hdf');
        fullpath = strcat(dest,filename);
        
        if readM==true;
            for i=1:nCadence:(nt+1);
            % Do the same for the moments, looping over each particle species
            for s=0:ns-1
                for k=1:5
                    temp = h5read(fullpath,strcat('/TimeStep/',num2str(i-1),'/Pset/',num2str(s),'/ZoneMoments/',momu{k}));
                    M.(pname{s+1}).(moml{k})(rcell(p,1):rcell(p,2),rcell(p,3):rcell(p,4),rcell(p,5):rcell(p,6),ceil(i/nCadence)) ...
                        = permute(temp(2:end-1,2:end-1,2:end-1),[3 2 1]);
                end % moment loop
            end % species loop
            end
        end
        
end % zone loop

close(bar)
clear temp

display('Size of n ')
display(size(M.protons.nB))

%% 2D plot
t=1;
cellSize=dx(1);

%cellSize=0.465802;
density =  squeeze(M.protons.nB(20,:,:,t)+M.blob.nB(20,:,:,t));

clims = [ 0.9 max(M.protons.nB(:)+M.blob.nB(:)) ]
%xx=(1:810)*cellSize;
%yy=(1:810)*cellSize;
xx=(1:nd(2))*cellSize;

imagesc(xx,xx,density)
set(gca,'YDir','reverse');
ylabel('z /\rho_i')
xlabel('y /\rho_i')
title('density')
colorbar('vertical')
caxis(clims)
%%
cellSize=dx(1);
cs=cellSize;
[ x, y, z] = meshgrid((0:0.5:(nd(2)-0.5)*0.5)*cs,(0:0.5:(nd(1)-0.5)*0.5)*cs,(0:0.5:(nd(3)-0.5)*0.5)*cs);

t=11;
nn=M.blob.nB(:,:,:,t);
n= M.protons.nB(:,:,:,t);
v=n+nn;


%

ymin = min(x(:)); 
xmin = min(y(:)); 
zmin = min(z(:));
nx=nd(1);
ny=nd(2);
nz=nd(3);
ymax = max(x(:)); 
xmax = max(y(:)); 
zmax = max(z(:));
alt=5/100*zmax; % raise the angle-plane
% Slice Plane at an Angle to the X-Axes
% surf(X,Y,Z) If X and Y are vectors, length(X) = n and length(Y)  = m,
% where [m,n] = size(Z)
hslice = surf(linspace(xmin,xmax,nx),...
	linspace(ymin,ymax,ny),...
    (zmax/2+alt)*ones(ny,nx));
	%((nx+ny)*cs*(0.204)/(xmax)+alt)*ones(ny,nx));
%
rotate(hslice,[0.5,0,0.],45)
xd = get(hslice,'XData');
yd = get(hslice,'YData');
zd = get(hslice,'ZData');
delete(hslice)
h=slice(x,y,z,v,yd,xd,zd)
set(h,'FaceColor','interp',...
	'EdgeColor','none',...
	'DiffuseStrength',.8)

%
hold on
yslice=[ymax]; 
hx = slice(x,y,z,v,yslice,[],[]);
set(hx,'FaceColor','interp','EdgeColor','none')


hy = slice(x,y,z,v,[],xmax,[]);
set(hy,'FaceColor','interp','EdgeColor','none')

hz = slice(x,y,z,v,[],[],zmin);
set(hz,'FaceColor','interp','EdgeColor','none')

% Define the View
%daspect([1,1,1])
axis tight
box on
view(-42,22)
camzoom(0.98)
camproj perspective
% Add Lighting and Specify Colors
lightangle(-48,15)
%colormap (jet(24))
set(gcf,'Renderer','zbuffer')

% Customize the Colormap
%colormap (flipud(jet(24)))
%colorbar('horiz')
colorbar('vertical')

 %caxis([min(M.protons.n(:)),max(M.test1.n(:))+max(M.protons.n(:))])
 caxis([min(v(:)),max(M.protons.nB(:)+M.blob.nB(:))])
 %caxis([min(E.x(:)),max(E.x(:))])
 
 xlabel('y')
ylabel('x')
zlabel('z')
title(strcat(date(),' Blobs3D', ' t= ',num2str((t-1)*nCadence*dt),'\Omega_i^{-1}'),'FontSize',14)
